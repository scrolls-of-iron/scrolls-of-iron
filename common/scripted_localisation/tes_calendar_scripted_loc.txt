defined_text = {
	name = GetCalendarTooltip
	text = { 
		trigger = {
			always = yes
		}
		localization_key = calendar_tooltip
	}
}

defined_text = {
	name = GetCalendarText
	text = { 
		trigger = {
			always = yes
		}
		localization_key = calendar_text_1
	}
	text = { 
		trigger = {
			always = no
		}
		localization_key = calendar_text_2
	}
}

defined_text = {
	name = GetCalendarYearText
	text = { #1E 123
		trigger = {
			NOT = { has_country_flag = year_chronological }
			NOT = { has_country_flag = year_combined }
		}
		localization_key = calendar_era_year_text
	}
	text = { #1E 123 (1234)
		trigger = {
			has_country_flag = year_combined
		}
		localization_key = calendar_combined_year_text
	}
	text = { #1234
		trigger = {
			has_country_flag = year_chronological
		}
		localization_key = calendar_chronological_year_text
	}
}

defined_text = {
	name = GetCalendarHourText
	text = { 
		trigger = {
			NOT = { has_country_flag = clock_12_hour }
		}
		localization_key = calendar_24h_text
	}
	text = { 
		trigger = {
			has_country_flag = clock_12_hour
			check_variable = { global.calendar_array^0 < 12 }
		}
		localization_key = calendar_12h_text
	}
	text = { 
		trigger = {
			has_country_flag = clock_12_hour
			check_variable = { global.calendar_array^0 = 12 }
		}
		localization_key = calendar_12h_text_12pm
	}
	text = { 
		trigger = {
			has_country_flag = clock_12_hour
			check_variable = { global.calendar_array^0 = 13 }
		}
		localization_key = calendar_12h_text_1pm
	}
	text = { 
		trigger = {
			has_country_flag = clock_12_hour
			check_variable = { global.calendar_array^0 = 14 }
		}
		localization_key = calendar_12h_text_2pm
	}
	text = { 
		trigger = {
			has_country_flag = clock_12_hour
			check_variable = { global.calendar_array^0 = 15 }
		}
		localization_key = calendar_12h_text_3pm
	}
	text = { 
		trigger = {
			has_country_flag = clock_12_hour
			check_variable = { global.calendar_array^0 = 16 }
		}
		localization_key = calendar_12h_text_4pm
	}
	text = { 
		trigger = {
			has_country_flag = clock_12_hour
			check_variable = { global.calendar_array^0 = 17 }
		}
		localization_key = calendar_12h_text_5pm
	}
	text = { 
		trigger = {
			has_country_flag = clock_12_hour
			check_variable = { global.calendar_array^0 = 18 }
		}
		localization_key = calendar_12h_text_6pm
	}
	text = { 
		trigger = {
			has_country_flag = clock_12_hour
			check_variable = { global.calendar_array^0 = 19 }
		}
		localization_key = calendar_12h_text_7pm
	}
	text = { 
		trigger = {
			has_country_flag = clock_12_hour
			check_variable = { global.calendar_array^0 = 20 }
		}
		localization_key = calendar_12h_text_8pm
	}
	text = { 
		trigger = {
			has_country_flag = clock_12_hour
			check_variable = { global.calendar_array^0 = 21 }
		}
		localization_key = calendar_12h_text_9pm
	}
	text = { 
		trigger = {
			has_country_flag = clock_12_hour
			check_variable = { global.calendar_array^0 = 22 }
		}
		localization_key = calendar_12h_text_10pm
	}
	text = { 
		trigger = {
			has_country_flag = clock_12_hour
			check_variable = { global.calendar_array^0 = 23 }
		}
		localization_key = calendar_12h_text_11pm
	}
	text = { 
		trigger = {
			has_country_flag = clock_12_hour
			check_variable = { global.calendar_array^0 = 24 }
		}
		localization_key = calendar_12h_text_12am
	}
}