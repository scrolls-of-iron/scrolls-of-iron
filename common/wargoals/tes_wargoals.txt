wargoal_types = {
	wargoal_conquest = {
		# PREV = original target country
		# ROOT = goal owner country

		allowed = {

		}
		
		available = {
		
		}
		
		take_states = {
			
		}
		
		generate_base_cost = 100		
		generate_per_state_cost = 15
		
		take_states_limit = 20
		take_states_cost = -20
		
		expire = 60
		
		threat = 2
	}
	
	wargoal_conquest_claim = {
		# PREV = original target country
		# ROOT = goal owner country
		
		take_states = {
			is_claimed_by = ROOT
			is_owned_by = PREV
			is_controlled_by = PREV
		}
		
		generate_base_cost = 50
		generate_per_state_cost = 10	
		
		take_states_limit = 50
		take_states_cost = -50
		
		threat = 0.15
	}
	
	wargoal_conquest_core = {
		# PREV = original target country
		# ROOT = goal owner country

		war_name = war_name_wargoal_conquest_core
		
		allowed = {
			#always = no
		}
		
		take_states = {
			is_core_of = ROOT
			is_owned_by = PREV
			is_controlled_by = PREV
		}
		
		generate_base_cost = 50
		generate_per_state_cost = 0
		
		take_states_limit = 50
		take_states_cost = -80
		
		
		threat = 0.1
	}

	wargoal_civil_war = {
		# PREV = original target country
		# ROOT = goal owner country

		war_name = war_name_wargoal_civil_war

		allowed = {
			always = no
		}
		
		available = {
		
		}
		
		take_states = {
			
		}
				
		threat = 1
		annex_threat_factor = 0.1
		annex_cost = -75
	}

	wargoal_purge_vampires = {
		# PREV = original target country
		# ROOT = goal owner country

		war_name = war_name_wargoal_purge_vampires

		allowed = {
			
		}
		
		available = {
			PREV = {
				has_government = race_vampire
			}
		}
		
		take_states = {
			
		}
		
		generate_base_cost = 50		
		generate_per_state_cost = 0
		
		take_states_limit = 1
		take_states_cost = -20
		
		expire = 60
		
		threat = 0.1
	}
}
