#These tiers are for formable nations, so with default game rules, tier 0 countries can't form nations, and others can only form countries of an equal or higher tier.	Tier 0 = non-countries, such as the uncivilized land tag or planes of oblivion.		Tier 1 - all countries not of any of the other tiers, for example Daggerfall.		Tier 2 - regional formables, for example Glenumbra or the Gold Coast.		Tier 3 - provincial formables, for example High Rock or Cyrodiil.		Tier 4 - continental formables, for example Tamriel.
is_tier_0_country = {
	hidden_trigger = {
		OR = {
			has_country_flag = tier_0_country		#For submods, so you can designate your country as a specific tier without needing to modify this file and create incompatibilities.
		}
	}
}

is_tier_1_country = {
	hidden_trigger = {
		is_tier_0_country = no
		is_tier_2_country = no
		is_tier_3_country = no
		is_tier_4_country = no
	}
}

is_tier_2_country = {
	hidden_trigger = {
		OR = {
			has_country_flag = tier_2_country		#For submods, so you can designate your country as a specific tier without needing to modify this file and create incompatibilities.
			original_tag = GLE
		}
	}
}

is_tier_3_country = {
	hidden_trigger = {
		OR = {
			has_country_flag = tier_3_country		#For submods, so you can designate your country as a specific tier without needing to modify this file and create incompatibilities.
			original_tag = HRK
		}
	}
}

is_tier_4_country = {
	hidden_trigger = {
		OR = {
			has_country_flag = tier_4_country		#For submods, so you can designate your country as a specific tier without needing to modify this file and create incompatibilities.
			original_tag = TAM
		}
	}
}

can_form_tier_2 = {
	hidden_trigger = {
		OR = {
			AND = {
				has_game_rule = {
					rule = tes_gamerule_formable_nations
					option = tes_gamerule_formable_nations_unrestricted
				}
				NOT = {
					is_tier_0_country = yes
				}
			}
			AND = {
				has_game_rule = {
					rule = tes_gamerule_formable_nations
					option = tes_gamerule_formable_nations_horizontal
				}
				OR = {
					is_tier_1_country = yes
					is_tier_2_country = yes
				}
			}
			AND = {
				has_game_rule = {
					rule = tes_gamerule_formable_nations
					option = tes_gamerule_formable_nations_upward_only
				}
				is_tier_1_country = yes
			}
		}
	}
}

can_form_tier_3 = {
	hidden_trigger = {
		OR = {
			AND = {
				has_game_rule = {
					rule = tes_gamerule_formable_nations
					option = tes_gamerule_formable_nations_unrestricted
				}
				is_tier_0_country = no
			}
			AND = {
				has_game_rule = {
					rule = tes_gamerule_formable_nations
					option = tes_gamerule_formable_nations_horizontal
				}
				OR = {
					is_tier_1_country = yes
					is_tier_2_country = yes
					is_tier_3_country = yes
				}
			}
			AND = {
				has_game_rule = {
					rule = tes_gamerule_formable_nations
					option = tes_gamerule_formable_nations_upward_only
				}
				OR = {
					is_tier_1_country = yes
					is_tier_2_country = yes
				}
			}
		}
	}
}

can_form_tier_4 = {
	hidden_trigger = {
		OR = {
			AND = {
				has_game_rule = {
					rule = tes_gamerule_formable_nations
					option = tes_gamerule_formable_nations_unrestricted
				}
				NOT = {
					is_tier_0_country = yes
				}
			}
			AND = {
				has_game_rule = {
					rule = tes_gamerule_formable_nations
					option = tes_gamerule_formable_nations_horizontal
				}
				OR = {
					is_tier_1_country = yes
					is_tier_2_country = yes
					is_tier_3_country = yes
					is_tier_4_country = yes
				}
			}
			AND = {
				has_game_rule = {
					rule = tes_gamerule_formable_nations
					option = tes_gamerule_formable_nations_upward_only
				}
				OR = {
					is_tier_1_country = yes
					is_tier_2_country = yes
					is_tier_3_country = yes
				}
			}
		}
	}
}