on_actions = {
	on_startup = {
		effect = {
			every_country = {
				limit = {
					NOT = { original_tag = AAA }
					NOT = { original_tag = WNT } #TEMP - If winterhold has an army, the game crashes 2-4 hours in. WHy???
				}
				generate_army = yes
			}
			AAA = {
				calendar_setup = yes
				country_event = {
					id = tes_calendar.1 #The AI on scripted gui doesn't run on day one, so events need to handle the calendar system for the first day
					hours = 1
				}
				country_event = {
					id = tes_calendar.3 #Sets a flag to stop the previous event from triggering
					hours = 14
				}
			}
			every_country = {
				create_population_array = yes
				generate_ruler_titles_array = yes #this array keeps track of which countries one ruler rules over, so when hovering over the ruler's name in diplomacyview, it will list all countries they rule
			}
			every_country = {
				limit = {
					has_no_ruler = yes
				}
				generate_random_ruler = yes
			}
			every_state = { create_racial_piechart = yes }
			every_country = {
				limit = {
					NOT = { tag = AAA }
				}
				set_variable = { treasury = 1000 }
			}
			236 = {
				owner = {
					load_oob = "oob_navy_mtg_236_3E430"
				}
			}
		}
	}

	on_release_as_free = {
		effect = {
			ROOT = { 
				set_country_flag = { flag = do_population_array value = 1 days = 1 } 
				calculate_national_racial_demographics = yes
				if = { 
					limit = {
						has_no_ruler = yes
					}
					generate_random_ruler = yes
				}
				generate_army = yes 
				diplomatic_relation = {
					country = AAA
					relation = military_access
					active = yes
				}
			}
			FROM = {
				set_country_flag = { flag = do_population_array value = 1 days = 1 } 
				calculate_national_racial_demographics = yes		
			}
			every_country = {
				limit = {
					has_country_flag = do_population_array
				}
				create_population_array = yes
			}
		}
	}
	
	on_release_as_puppet = {
		effect = {
			ROOT = { 
				set_country_flag = { flag = do_population_array value = 1 days = 1 } 
				calculate_national_racial_demographics = yes
				if = { 
					limit = {
						has_no_ruler = yes
					}
					generate_random_ruler = yes
				}
				generate_army = yes 
				diplomatic_relation = {
					country = AAA
					relation = military_access
					active = yes
				}
			}
			FROM = {
				set_country_flag = { flag = do_population_array value = 1 days = 1 } 
				calculate_national_racial_demographics = yes		
			}
			every_country = {
				limit = {
					has_country_flag = do_population_array
				}
				create_population_array = yes
			}
		}
	}

	on_puppet = {
		effect = {
			ROOT = { 
				set_country_flag = { flag = do_population_array value = 1 days = 1 } 
				calculate_national_racial_demographics = yes
				if = { 
					limit = {
						has_no_ruler = yes
					}
					generate_random_ruler = yes
				}
				generate_army = yes 
				diplomatic_relation = {
					country = AAA
					relation = military_access
					active = yes
				}
			}
			FROM = {
				set_country_flag = { flag = do_population_array value = 1 days = 1 } 
				calculate_national_racial_demographics = yes		
			}
			every_country = {
				limit = {
					has_country_flag = do_population_array
				}
				create_population_array = yes
			}
		}
	}

	on_liberate = {
		effect = {
			ROOT = { 
				set_country_flag = { flag = do_population_array value = 1 days = 1 } 
				calculate_national_racial_demographics = yes
				if = { 
					limit = {
						has_no_ruler = yes
					}
					generate_random_ruler = yes
				}
				generate_army = yes 
				diplomatic_relation = {
					country = AAA
					relation = military_access
					active = yes
				}
			}
			FROM = {
				set_country_flag = { flag = do_population_array value = 1 days = 1 } 
				calculate_national_racial_demographics = yes		
			}
			every_country = {
				limit = {
					has_country_flag = do_population_array
				}
				create_population_array = yes
			}
		}
	}
	
	on_annex = { 
		effect = {
			if = {
				limit = {
					FROM = { tag = AAA }
				}
				FROM = { transfer_state = 191 } #Placeholder state, after all states are finished I will add an empty 2-pixel state somewhere to store them - they need to be alive to run monthly commands and things like that.
			}
			ROOT = {
				create_population_array = yes
				calculate_national_racial_demographics = yes
			}
		}
	}

	on_state_control_changed = {
		effect = {
			if = { #Fall of Orsinium event
				limit = { #Will only trigger when Orsinium is conquered from Orcs by non-orcs
					FROM = {
						has_government = race_orsimer
						has_war_with = ROOT
					}
					ROOT = {
						NOT = { 
							has_government = race_orsimer 
						}
					}
					FROM.FROM = {
						state = 105 #Nova Orsinium
						is_owned_by = PREV.FROM
					}
				}
				FROM = { #FROM is scoped here so the FROM scope will be retained in the event
					ROOT = { country_event = tes_orsinium.1 }
				}
			}
		}
	}
}