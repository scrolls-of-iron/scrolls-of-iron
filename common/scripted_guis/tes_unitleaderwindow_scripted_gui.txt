scripted_gui = {
	unitleaderwindow_recruit_general = {
		window_name = "unitleaderwindow_recruit_general_container"
		context_type = player_context
		parent_window_name = armyleaderwindow
		visible = {
			always = yes
		}
		ai_enabled = {
			always = no
		}
		effects = {
			recruit_general_button_click = {
				generate_random_general = yes
				subtract_from_variable = { treasury = modifier@recruit_general_cost }
			}
		}
	}
}