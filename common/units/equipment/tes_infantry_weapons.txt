equipments = {
	equipment_infantry_weapons = {
		is_archetype = yes
		picture = archetype_infantry_weapons
		is_buildable = no
		type = infantry
		group_by = archetype
		
		interface_category = interface_category_land
		
		active = yes
		
		#Misc Abilities
		reliability = 0.25
		maximum_speed = 4

		#Defensive Abilities
		defense = 0
		breakthrough = 2
		hardness = 0
		armor_value = 0

		#Offensive Abilities
		soft_attack = 3
		hard_attack = 0.5
		ap_attack = 1
		air_attack = 0

		#Space taken in convoy
		lend_lease_cost = 1
		
		build_cost_ic = 0.43
	}

	equipment_infantry_weapons_primitive = {
		archetype = equipment_infantry_weapons
		priority = 1
		visual_level = 0

		resources = {
			resource_wood = 3
		}
	}

	equipment_infantry_weapons_chitin = {
		archetype = equipment_infantry_weapons
		parent = equipment_infantry_weapons_primitive
		priority = 5
		visual_level = 1

		#Misc Abilities
		reliability = 0.35
	}

	equipment_infantry_weapons_iron = {	
		archetype = equipment_infantry_weapons
		parent = equipment_infantry_weapons_chitin
		priority = 10
		visual_level = 2
		
		#Misc Abilities
		reliability = 0.5

		#Offensive Abilities
		soft_attack = 6
		hard_attack = 1
		ap_attack = 4
		air_attack = 0

		build_cost_ic = 0.50
		
		resources = {
			resource_wood = 1
			resource_iron = 2
		}
	}

	equipment_infantry_weapons_steel = {
		archetype = equipment_infantry_weapons
		parent = equipment_infantry_weapons_iron
		priority = 10
		visual_level = 3
		
		#Misc Abilities
		reliability = 0.65

		#Offensive Abilities
		soft_attack = 6
		hard_attack = 1
		ap_attack = 4
		air_attack = 0

		build_cost_ic = 0.50
		
		resources = {
			resource_wood = 1
			resource_iron = 2
			resource_corundum = 1
		}
	}

	equipment_infantry_weapons_orichalcum = {
		archetype = equipment_infantry_weapons
		parent = equipment_infantry_weapons_steel
		priority = 10
		visual_level = 5
		
		#Misc Abilities
		reliability = 0.65

		#Offensive Abilities
		soft_attack = 6
		hard_attack = 1
		ap_attack = 4
		air_attack = 0

		build_cost_ic = 0.50
		
		resources = {
			resource_wood = 1
			resource_orichalcum = 3
		}
	}

	equipment_infantry_weapons_moonstone = {
		archetype = equipment_infantry_weapons
		parent = equipment_infantry_weapons_orichalcum
		priority = 10
		visual_level = 6
		
		#Misc Abilities
		reliability = 0.65

		#Offensive Abilities
		soft_attack = 6
		hard_attack = 1
		ap_attack = 4
		air_attack = 0

		build_cost_ic = 0.50
		
		resources = {
			resource_wood = 1
			resource_moonstone = 2
			resource_quicksilver = 1
		}
	}

	equipment_infantry_weapons_malachite = {
		archetype = equipment_infantry_weapons
		parent = equipment_infantry_weapons_moonstone
		priority = 10
		visual_level = 7
		
		#Misc Abilities
		reliability = 0.65

		#Offensive Abilities
		soft_attack = 6
		hard_attack = 1
		ap_attack = 4
		air_attack = 0

		build_cost_ic = 0.50
		
		resources = {
			resource_wood = 1
			resource_malachite = 3
		}
	}

	equipment_infantry_weapons_ebony = {
		archetype = equipment_infantry_weapons
		parent = equipment_infantry_weapons_malachite
		priority = 10
		visual_level = 8
		
		#Misc Abilities
		reliability = 0.65

		#Offensive Abilities
		soft_attack = 6
		hard_attack = 1
		ap_attack = 4
		air_attack = 0

		build_cost_ic = 0.50
		
		resources = {
			resource_wood = 1
			resource_ebony = 3
		}
	}

	equipment_infantry_weapons_stalhrim = {
		archetype = equipment_infantry_weapons
		parent = equipment_infantry_weapons_ebony
		priority = 10
		visual_level = 9
		
		#Misc Abilities
		reliability = 0.65

		#Offensive Abilities
		soft_attack = 6
		hard_attack = 1
		ap_attack = 4
		air_attack = 0

		build_cost_ic = 0.50
		
		resources = {
			resource_wood = 1
			resource_stalhrim = 3
		}
	}

	equipment_infantry_weapons_mithril = {
		archetype = equipment_infantry_weapons
		parent = equipment_infantry_weapons_stalhrim
		priority = 10
		visual_level = 10
		
		#Misc Abilities
		reliability = 0.65

		#Offensive Abilities
		soft_attack = 6
		hard_attack = 1
		ap_attack = 4
		air_attack = 0

		build_cost_ic = 0.50
		
		resources = {
			resource_wood = 1
			resource_mithril = 3
		}
	}

	equipment_infantry_weapons_adamantium = {
		archetype = equipment_infantry_weapons
		parent = equipment_infantry_weapons_mithril
		priority = 10
		visual_level = 11
		
		#Misc Abilities
		reliability = 0.65

		#Offensive Abilities
		soft_attack = 6
		hard_attack = 1
		ap_attack = 4
		air_attack = 0

		build_cost_ic = 0.50
		
		resources = {
			resource_wood = 1
			resource_adamantium = 3
		}
	}
}