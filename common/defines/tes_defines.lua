-- NCOUNTRY
NDefines.NCountry.POPULATION_YEARLY_GROWTH_BASE = 0.001
NDefines.NCountry.SUPPLY_FROM_DAMAGED_INFRA = 0.15               											-- damaged infrastructure counts as this in supply calcs

-- NBUILDINGS
--		Infra resource boost
NDefines.NBuildings.INFRASTRUCTURE_RESOURCE_BONUS = 0.05													-- Bonus resource percent per infrastructure level
NDefines.NBuildings.INFRA_TO_SUPPLY = 0.5																	-- Supply from infrastructure

-- NDIPLOMACY
--		Min/max opinion
NDefines.NDiplomacy.MAX_OPINION_VALUE = 999																	-- Maximum opinion
NDefines.NDiplomacy.MIN_OPINION_VALUE = -999																-- Minimum opinion

-- NFRONTEND
--		Camera height
NDefines.NFrontend.CAMERA_MIN_HEIGHT = 30.0																	-- Minimum camera height
NDefines.NFrontend.CAMERA_MAX_HEIGHT = 3520.0																-- Maximum camera height

--		Set default camera position to the middle of the map
NDefines.NFrontend.CAMERA_LOOKAT_X = 2304.0 						
NDefines.NFrontend.CAMERA_LOOKAT_Y = 3500.0
NDefines.NFrontend.CAMERA_LOOKAT_Z = 1408.0
NDefines.NFrontend.CAMERA_START_X = 2304.0						
NDefines.NFrontend.CAMERA_START_Y = 3500.0					
NDefines.NFrontend.CAMERA_START_Z = 1408.0
NDefines.NFrontend.CAMERA_END_X = 2304.0						
NDefines.NFrontend.CAMERA_END_Y = 3500.0
NDefines.NFrontend.CAMERA_END_Z = 1408.0
NDefines.NFrontend.FRONTEND_POS_X = 2304.0
NDefines.NFrontend.FRONTEND_POS_Y = 3500.0
NDefines.NFrontend.FRONTEND_POS_Z = 1408.0
NDefines.NFrontend.FRONTEND_LOOK_X = 2304.0
NDefines.NFrontend.FRONTEND_LOOK_Y = 3500.0
NDefines.NFrontend.FRONTEND_LOOK_Z = 1408.0
NDefines.NFrontend.MP_OPTIONS_POS_X = 2304.0
NDefines.NFrontend.MP_OPTIONS_POS_Y = 3500.0
NDefines.NFrontend.MP_OPTIONS_POS_Z = 1408.0
NDefines.NFrontend.MP_OPTIONS_LOOK_X = 2304.0
NDefines.NFrontend.MP_OPTIONS_LOOK_Y = 3500.0
NDefines.NFrontend.MP_OPTIONS_LOOK_Z = 1408.0

-- NGAME
--		Start and end dates
NDefines.NGame.START_DATE = "4246.1.1.12"																		-- Starts 3E 430
NDefines.NGame.END_DATE = "9000.1.1.1"																			-- No end date
	
NDefines.NGame.MAP_SCALE_PIXEL_TO_KM = 0.688															-- Yes, we did the math

NDefines.NGame.FUEL_RESOURCE = "resource_food"															-- Food replaces fuel


-- NGRAPHICS
NDefines.NGraphics.BORDER_COLOR_CUSTOM_HIGHLIGHTS = { -- State highlight colors
		--[[ Groups of 4 numbers are RGBA.
			If two colors are both active on a border, (because one province is
				part of a group using one color, and the other province is part
				of another group), then the color that comes first in this list
				is the color that will be used. ]]
		0.0, 0.61, 0.75, 1.0, -- 0: mouse hover
		0.0, 0.45, 0.05, 1.0,   -- 1: dark green				for peace offers, represents territory granted to you	
		0.15, 0.8, 0.3, 1.0,   -- 2: green						for peace offers, represents territory granted to subjects									vanilla comment: good, while active
		0.0, 0.4, 0.8, 1.0,   -- 3: blue							for peace offers, represents territory granted to allies									vanilla comment: good, while passive
		1.0, 0.62, 0.33, 1.0,   -- 4: neutral color			for peace offers, represents territory granted to neutral countries
		0.8, 0.3, 0.0, 1.0,   -- 5: orange						for peace offers, represents territory surrendered to secondary enemies									vanilla comment: bad, while passive
		1.0, 0.06, 0.0, 1.0,  -- 6: red							for peace offers, represents territory surrendered to primary enemy									vanilla comment: bad, while active
		1.0, 1.0, 1.0, 1.0,   -- 7: white
		0.0, 0.0, 0.0, 1.0,   -- 8: black
		0.8, 0.8, 0.0, 1.0,   -- 9: yellow
}

--		Flags
NDefines.NGraphics.COUNTRY_FLAG_TEX_WIDTH = 200														-- Large flag width					-- Large flags are 200x200
NDefines.NGraphics.COUNTRY_FLAG_TEX_HEIGHT = 200														-- Large flag height					-- Large flags are 200x200
NDefines.NGraphics.COUNTRY_FLAG_MEDIUM_TEX_WIDTH = 200										-- Medium flag width				-- Medium flags are 100x200
NDefines.NGraphics.COUNTRY_FLAG_MEDIUM_TEX_HEIGHT = 200										-- Medium flag height				-- Medium flags are 100x200
NDefines.NGraphics.COUNTRY_FLAG_SMALL_TEX_WIDTH = 200											-- Small flag width					-- Small flags are 200x132
NDefines.NGraphics.COUNTRY_FLAG_SMALL_TEX_HEIGHT = 200										-- Small flag height					-- Small flags are 200x132

NDefines.NGraphics.COUNTRY_FLAG_TEX_MAX_SIZE = 2048
NDefines.NGraphics.COUNTRY_FLAG_SMALL_TEX_MAX_SIZE = 2048
NDefines.NGraphics.COUNTRY_FLAG_STRIPE_TEX_MAX_WIDTH = 200
NDefines.NGraphics.COUNTRY_FLAG_STRIPE_TEX_MAX_HEIGHT = 16000
NDefines.NGraphics.COUNTRY_FLAG_LARGE_STRIPE_MAX_WIDTH = 200
NDefines.NGraphics.COUNTRY_FLAG_LARGE_STRIPE_MAX_HEIGHT = 16000

--		Country color adjustment
--NDefines.NGraphics.COUNTRY_COLOR_HUE_MODIFIER = 0.0												-- Don't modify hue
--NDefines.NGraphics.COUNTRY_COLOR_SATURATION_MODIFIER = 0.8
--NDefines.NGraphics.COUNTRY_COLOR_BRIGHTNESS_MODIFIER = 0.8
--NDefines.NGraphics.COUNTRY_UI_COLOR_HUE_MODIFIER = 0.0											-- Don't modify hue
--NDefines.NGraphics.COUNTRY_UI_COLOR_SATURATION_MODIFIER = 0.8
--NDefines.NGraphics.COUNTRY_UI_COLOR_BRIGHTNESS_MODIFIER = 0.8

--NDefines.NGraphics.GRADIENT_BORDERS_OUTLINE_CUTOFF_COUNTRY = 1.0

NDefines.NGraphics.MINIMUM_PROVINCE_SIZE_IN_PIXELS = 1											-- Provinces below minimum size don't cause any actual problems, just errors in the error log. Increase as needed to identify undersized provinces.

NDefines.NGraphics.RIVER_FADE_FROM = 10.0
NDefines.NGraphics.RIVER_FADE_TO = 1.0

--		Camera
NDefines.NGraphics.CAMERA_OUTSIDE_MAP_DISTANCE_TOP = 150.0																		-- Prevents players from seeing the empty sky above/below the map borders
NDefines.NGraphics.CAMERA_OUTSIDE_MAP_DISTANCE_BOTTOM = 150.0

-- NMAPMODE
NDefines.NMapMode.FACTIONS_COLOR_NOT_MEMBER = { 0.6, 0.6, 0.6, 0.1 }

NDefines.NMapMode.PLAYER_MAPMODE_NOT_SELECTED_COUNTRY_TRANSPARENCY = 0.25														-- How much is the country colors faded out, for countries that are not occupied by the any player.

NDefines.NMapMode.MAP_MODE_INFRA_RANGE_COLOR_FROM = { 0, 0, 1, 1 }																-- Color range for infrastructure map mode.
NDefines.NMapMode.MAP_MODE_INFRA_RANGE_COLOR_TO = { 0, 1, 0, 1 }

NDefines.NMapMode.MAP_MODE_MANPOWER_RANGE_MAX = 100000																			-- When a state has that much manpower, it will be colored with the color MAP_MODE_MANPOWER_RANGE_COLOR_TO. Everything below that will have an interpolated color.
NDefines.NMapMode.MAP_MODE_MANPOWER_RANGE_COLOR_FROM = { 0, 0, 0.5, 1 }															-- Color range for manpower map mode.
NDefines.NMapMode.MAP_MODE_MANPOWER_RANGE_COLOR_TO = { 0, 1, 0, 1 }

NDefines.NMapMode.MAP_MODE_IDEOLOGY_COLOR_TRANSPARENCY = 1

NDefines.NMapMode.MAP_MODE_TERRAIN_TRANSPARENCY = 1																				-- How much transparent are the province colors in the simplified terrain map mode
NDefines.NMapMode.MAP_MODE_NAVAL_TERRAIN_TRANSPARENCY = 1																		-- How much transparent are the SEA province colors in the simplified terrain map mode

NDefines.NMapMode.GMT_OFFSET = 2112																								-- Displayed time is that of the center of the map (Imperial City) (For some reason setting it to the actual center coord, 2304, makes it offset by like an hour!!)  Vanilla value is 2793 (Also defined in standardfuncsgfx.fxh)
NDefines.NMapMode.SOUTH_POLE_OFFSET = 0.2																						-- Vanilla value is 0.17 (Also defined in standardfuncsgfx.fxh)
NDefines.NMapMode.NORTH_POLE_OFFSET = 0.9																						-- Vanilla value is 0.93


-- NMILITARY
NDefines.NMilitary.STRATEGIC_SPEED_BASE = 5		               																 	-- Speed of strategic redeployment
NDefines.NMilitary.STRATEGIC_INFRA_SPEED = 1           	    																    -- Max of additional speed gained trouh=gh level for strategic redeployment per infra
NDefines.NMilitary.STRATEGIC_REDEPLOY_ORG_RATIO = 0.9																			-- Ratio of max org while strategic redeployment


-- NPOLITICS
NDefines.NPolitics.BASE_POLITICAL_POWER_INCREASE = 0

-- NTRADE
NDefines.NTrade.BASE_TRADE_FACTOR = 25																							-- 150 in vanilla