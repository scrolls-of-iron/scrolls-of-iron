Resource Distribution:

Wood should be distributed based on trees.bmp - divide number of light forest/jungle pixels by 150, divide number of dense forest/jungle pixels by 100.

For Skyrim, count every ore vein as one resource, every ore as 1/2 of a resource, every ingot as 2/3 of a resource. For ores located in a city's blacksmith, divide by 2 and distribute to nearby states.
Split ores located in Blackreach 3 ways between Raldbthar, Mzinchaleft, and Alftand