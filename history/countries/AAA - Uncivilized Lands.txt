﻿log = "([?global.calendar_array^4]E[?global.calendar_array^3])[?global.year].[?global.calendar_array^2].[?global.calendar_array^1].[?global.calendar_array^0] - Loading country history for [THIS.GetTag] ([THIS.GetName])"

capital = 191
set_variable = { capital_state = 191 }

set_research_slots = 0

set_stability = 0.0
set_variable = { base_stability = 0.0 }
set_war_support = 0.0
set_variable = { base_war_support = 0.0 }

add_ideas = {
	AAA_hidden_idea
}

set_politics = {
	ruling_party = race_nord
	last_election = "4247.1.1"
	election_frequency = 48
	elections_allowed = no
}

generate_global_state_racial_demographics = yes
every_country = {
	limit = { exists = yes }
	calculate_national_racial_demographics = yes
}
every_country = {
	limit = { NOT = { tag = AAA } }
	diplomatic_relation = {
		country = AAA
		relation = military_access
		active = yes
	}
}