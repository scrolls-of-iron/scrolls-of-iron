﻿log = "([?global.calendar_array^4]E[?global.calendar_array^3])[?global.year].[?global.calendar_array^2].[?global.calendar_array^1].[?global.calendar_array^0] - Loading country history for [THIS.GetTag] ([THIS.GetName])"

capital = 157 #Evermor
set_variable = { capital_state = 157 }

set_research_slots = 4

set_stability = 0.5
set_variable = { base_stability = 0.5 }
set_war_support = 0.5
set_variable = { base_war_support = 0.5 }

set_technology_primitive = yes
set_technology_iron = yes
set_technology_steel = yes

load_oob = unlock_infantry

add_dynamic_modifier = {
	modifier = dynamic_modifier_base_stats
	scope = THIS
}

add_ideas = {
	government_category_monarchy
	government_type_feudal_monarchy
	government_title_kingdom
}

set_politics = {
	ruling_party = race_breton
	last_election = "4247.1.1"
	election_frequency = 48
	elections_allowed = no
}

THIS = { #The temp variables don't stay around for the scripted effect without this scoping
	set_temp_variable = { random_ruler_race = 5 }
	generate_random_ruler = yes
}

ORS = { create_embargo = yes }