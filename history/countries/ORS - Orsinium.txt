﻿log = "([?global.calendar_array^4]E[?global.calendar_array^3])[?global.year].[?global.calendar_array^2].[?global.calendar_array^1].[?global.calendar_array^0] - Loading country history for [THIS.GetTag] ([THIS.GetName])"

capital = 105 #Nova Orsinium
set_variable = { capital_state = 105 }

set_research_slots = 4

set_stability = 0.5
set_variable = { base_stability = 0.5 }
set_war_support = 0.5
set_variable = { base_war_support = 0.5 }

set_technology_primitive = yes
set_technology_iron = yes
set_technology_steel = yes

load_oob = unlock_infantry

add_dynamic_modifier = {
	modifier = dynamic_modifier_base_stats
	scope = THIS
}

add_ideas = {
	government_category_monarchy
	government_type_orcish_monarchy
	government_title_kingdom
	extensive_conscription
}

set_politics = {
	ruling_party = race_orsimer
	last_election = "4247.1.1"
	election_frequency = 48
	elections_allowed = no
}

set_variable = { ruler_home_country = ORS } #Orsinium
set_variable = { ruler_sex = 1 } #Male
set_variable = { ruler_race = 10 } #Orsimer
set_variable = { ruler_name_group = 10 } #Orsimer
set_variable = { ruler_name = 71 } #Gothryd
set_variable = { ruler_lastname_group = 10 } #Orsimer
set_variable = { ruler_lastname_sex = 1 } #Male
set_variable = { ruler_lastname = 138 } #gro-Nagorm