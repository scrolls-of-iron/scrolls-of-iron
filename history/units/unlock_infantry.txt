﻿division_template = {
	name = "Infantry Battalion"
	division_names_group = DIVISION_NAMES_INFANTRY_BATTALION_GENERIC
	is_locked = yes
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
	}
	priority = 0
}

division_template = {
	name = "Infantry Regiment"
	division_names_group = DIVISION_NAMES_INFANTRY_REGIMENT_GENERIC
	is_locked = yes
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 0 y = 3 }
		infantry = { x = 0 y = 4 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
		infantry = { x = 1 y = 3 }
		infantry = { x = 1 y = 4 }
	}
	priority = 51
}