state = {
	id = 156
	name = "STATE_156" #Outer Evermor
	provinces = { 3555 10146 }
	manpower = 33861
	state_category = state_level_5
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = EVR
		add_core_of = EVR
		add_to_array = { provinces_array = 3555 }
		add_to_array = { provinces_array = 10146 }
		buildings = {
			infrastructure = 7
		}
	}
}