state = {
	id = 262
	name = "STATE_262" #Karthwatch
	provinces = { 9692 9852 }
	manpower = 19991
	state_category = state_level_4
	resources = {
		resource_iron = 1
	}
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = RCH
		add_core_of = RCH
		add_core_of = KAR
		victory_points = { 9852 3 } #Karthwatch
		add_to_array = { provinces_array = 9692 }
		add_to_array = { provinces_array = 9852 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 41 index = 2 } #Nord
		add_to_array = { array = demographics_array_race value = 59 index = 5 } #Reachman
		buildings = {
			infrastructure = 4
		}
	}
}