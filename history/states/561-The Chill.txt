state = {
	id = 561
	name = "STATE_561" #The Chill
	provinces = { 540 3103 3313 4075 4081 5544 5782 6939 6941 6942 6943 6944 }
	manpower = 499
	state_category = state_level_0
	resources = {
		resource_corundum = 1
		resource_iron = 5
		resource_orichalcum = 1
	}
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = AAA
		victory_points = { 6944 1 } #The Chill
		add_to_array = { provinces_array = 540 }
		add_to_array = { provinces_array = 3103 }
		add_to_array = { provinces_array = 3313 }
		add_to_array = { provinces_array = 4075 }
		add_to_array = { provinces_array = 4081 }
		add_to_array = { provinces_array = 5544 }
		add_to_array = { provinces_array = 5782 }
		add_to_array = { provinces_array = 6939 }
		add_to_array = { provinces_array = 6941 }
		add_to_array = { provinces_array = 6942 }
		add_to_array = { provinces_array = 6943 }
		add_to_array = { provinces_array = 6944 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 100 index = 2 } #Nord
	}
}