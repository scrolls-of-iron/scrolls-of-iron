state = {
	id = 70
	name = "STATE_70" #Thukhozodia
	provinces = { 3970 5152 7222 10087 10091 10094 10097 10256 }
	manpower = 25233
	state_category = state_level_5
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = SHM
		add_core_of = SHM
		add_core_of = BLA
		add_to_array = { provinces_array = 3970 }
		add_to_array = { provinces_array = 5152 }
		add_to_array = { provinces_array = 7222 }
		add_to_array = { provinces_array = 10087 }
		add_to_array = { provinces_array = 10091 }
		add_to_array = { provinces_array = 10094 }
		add_to_array = { provinces_array = 10097 }
		add_to_array = { provinces_array = 10256 }
		buildings = {
			infrastructure = 5
		}
	}
}