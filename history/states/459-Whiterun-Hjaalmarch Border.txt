state = {
	id = 459
	name = "STATE_459" #Whiterun-Hjaalmarch Border
	provinces = { 169 590 10569 }
	manpower = 7612
	state_category = state_level_3
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = WHI
		add_core_of = WHI
		add_core_of = ROR
		add_to_array = { provinces_array = 169 }
		add_to_array = { provinces_array = 590 }
		add_to_array = { provinces_array = 10569 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 43 index = 2 } #Nord
		add_to_array = { array = demographics_array_race value = 57 index = 5 } #Reachman
		buildings = {
			infrastructure = 3
		}
	}
}