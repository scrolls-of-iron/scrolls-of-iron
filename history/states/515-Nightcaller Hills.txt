state = {
	id = 515
	name = "STATE_515" #Nightcaller Hills
	provinces = { 2731 2984 6982 6997 7003 10947 }
	manpower = 1927
	state_category = state_level_1
	resources = {
		resource_corundum = 1
		resource_iron = 2
		resource_moonstone = 2
	}
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = PAL
		add_core_of = PAL
		victory_points = { 6982 3 } #Nightcaller
		add_to_array = { provinces_array = 2731 }
		add_to_array = { provinces_array = 2984 }
		add_to_array = { provinces_array = 6982 }
		add_to_array = { provinces_array = 6997 }
		add_to_array = { provinces_array = 7003 }
		add_to_array = { provinces_array = 10947 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 100 index = 2 } #Nord
		buildings = {
			infrastructure = 1
		}
	}
}