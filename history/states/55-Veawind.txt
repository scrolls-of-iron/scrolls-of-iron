state = {
	id = 55
	name = "STATE_55" #Veawind
	provinces = { 66 166 9809 9899 9900 9913 9915 9917 9923 9929 9933 9944 9954 }
	manpower = 29128
	state_category = state_level_5
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = NPT
		add_core_of = NPT
		add_to_array = { provinces_array = 66 }
		add_to_array = { provinces_array = 166 }
		add_to_array = { provinces_array = 9809 }
		add_to_array = { provinces_array = 9899 }
		add_to_array = { provinces_array = 9900 }
		add_to_array = { provinces_array = 9913 }
		add_to_array = { provinces_array = 9915 }
		add_to_array = { provinces_array = 9917 }
		add_to_array = { provinces_array = 9923 }
		add_to_array = { provinces_array = 9929 }
		add_to_array = { provinces_array = 9933 }
		add_to_array = { provinces_array = 9944 }
		add_to_array = { provinces_array = 9954 }
		buildings = {
			infrastructure = 4
		}
	}
}