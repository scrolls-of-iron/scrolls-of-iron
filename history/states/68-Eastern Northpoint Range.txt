state = {
	id = 68
	name = "STATE_68" #Eastern Northpoint Range
	provinces = { 1263 4600 }
	manpower = 1039
	state_category = state_level_0
	impassable = yes
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = NPT
		add_core_of = NPT
		add_core_of = NMR
	}
}