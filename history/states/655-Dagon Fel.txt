state = {
	id = 655
	name = "STATE_655" #Dagon Fel
	provinces = { 675 752 5721 }
	manpower = 13986
	state_category = state_level_4
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = DGF
		add_core_of = DGF
		victory_points = { 675 5 } #Dagon Fel
		add_to_array = { provinces_array = 675 }
		add_to_array = { provinces_array = 752 }
		add_to_array = { provinces_array = 5721 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 88 index = 2 } #Nord
		add_to_array = { array = demographics_array_race value = 4 index = 4 } #Breton
		add_to_array = { array = demographics_array_race value = 5 index = 8 } #Dunmer
		add_to_array = { array = demographics_array_race value = 3 index = 9 } #Orsimer
		buildings = {
			infrastructure = 4
			675 = { naval_base = 2 bunker = 1 }
		}
	}
}