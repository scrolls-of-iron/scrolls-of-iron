state = {
	id = 603
	name = "STATE_603" #Mara's Eye
	provinces = { 1167 3388 3974 9736 9798 10730 10876 }
	manpower = 16891
	state_category = state_level_4
	resources = {
		resource_corundum = 2
		resource_iron = 4
	}
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = EAM
		add_core_of = EAM
		add_core_of = MVS
		victory_points = { 10876 2 } #Gallows Rock
		add_to_array = { provinces_array = 1167 }
		add_to_array = { provinces_array = 3388 }
		add_to_array = { provinces_array = 3974 }
		add_to_array = { provinces_array = 9736 }
		add_to_array = { provinces_array = 9798 }
		add_to_array = { provinces_array = 10730 }
		add_to_array = { provinces_array = 10876 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 100 index = 2 } #Nord
		buildings = {
			infrastructure = 2
		}
	}
}