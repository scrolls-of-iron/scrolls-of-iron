state = {
	id = 346
	name = "STATE_346" #Northwest Jerall Mountains
	provinces = { 23 1902 }
	manpower = 4592
	state_category = state_level_0
	impassable = yes
	resources = {
		resource_moonstone = 1
		resource_orichalcum = 1
		resource_quicksilver = 2
	}
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = FAL
		add_core_of = FAL
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 100 index = 2 } #Nord
	}
}