state = {
	id = 62
	name = "STATE_62" #Edrald
	provinces = { 3511 9810 9930 9986 }
	manpower = 7621
	state_category = state_level_3
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = NPT
		add_core_of = NPT
		add_core_of = NMR
		add_to_array = { provinces_array = 3511 }
		add_to_array = { provinces_array = 9810 }
		add_to_array = { provinces_array = 9930 }
		add_to_array = { provinces_array = 9986 }
		buildings = {
			infrastructure = 3
		}
	}
}