state = {
	id = 445
	name = "STATE_445" #Whiterun-Reach Border
	provinces = { 2613 7336 }
	manpower = 3806
	state_category = state_level_2
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = WHI
		add_core_of = WHI
		add_core_of = ROR
		add_to_array = { provinces_array = 2613 }
		add_to_array = { provinces_array = 7336 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 51 index = 2 } #Nord
		add_to_array = { array = demographics_array_race value = 49 index = 5 } #Reachman
		buildings = {
			infrastructure = 2
		}
	}
}