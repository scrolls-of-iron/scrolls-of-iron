state = {
	id = 327
	name = "STATE_327" #South Shriekwind
	provinces = { 3557 7524 }
	manpower = 15895
	state_category = state_level_4
	resources = {
		resource_iron = 2
	}
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = FAL
		add_core_of = FAL
		add_to_array = { provinces_array = 3557 }
		add_to_array = { provinces_array = 7524 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 100 index = 2 } #Nord
		buildings = {
			infrastructure = 4
		}
	}
}