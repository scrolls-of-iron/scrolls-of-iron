state = {
	id = 698
	name = "STATE_698" #Vivec City - Hlaalu Canton
	provinces = { 7674 }
	manpower = 25153
	state_category = state_level_5
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = HLA
		add_core_of = HLA
		victory_points = { 7674 16 } #Vivec City - Hlaalu Canton
		add_to_array = { provinces_array = 7674 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 2 index = 1 } #Nibenese
		add_to_array = { array = demographics_array_race value = 4 index = 4 } #Breton
		add_to_array = { array = demographics_array_race value = 6 index = 7 } #Bosmer
		add_to_array = { array = demographics_array_race value = 84 index = 8 } #Dunmer
		add_to_array = { array = demographics_array_race value = 4 index = 9 } #Orsimer
		buildings = {
			infrastructure = 9
		}
	}
}