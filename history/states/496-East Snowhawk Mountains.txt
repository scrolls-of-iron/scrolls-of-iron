state = {
	id = 496
	name = "STATE_496" #East Snowhawk Mountains
	provinces = { 2422 }
	manpower = 989
	state_category = state_level_0
	impassable = yes
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = HJL
		add_core_of = HJL
		add_core_of = SNO
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 100 index = 2 } #Nord
	}
}