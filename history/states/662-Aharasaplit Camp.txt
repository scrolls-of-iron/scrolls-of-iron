state = {
	id = 662
	name = "STATE_662" #Aharasaplit Camp
	provinces = { 1447 }
	manpower = 1424
	state_category = state_level_1
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = AHA
		add_core_of = AHA
		victory_points = { 1447 1 } #Aharasaplit Camp
		add_to_array = { provinces_array = 1447 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 100 index = 8 } #Dunmer
		buildings = {
			infrastructure = 4
		}
	}
}