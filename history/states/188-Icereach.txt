state = {
	id = 188
	name = "STATE_188" #Icereach
	provinces = { 297 383 1599 7023 }
	manpower = 13014
	state_category = state_level_4
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = FAR
		add_core_of = FAR
		add_core_of = JEH
		add_to_array = { provinces_array = 297 }
		add_to_array = { provinces_array = 383 }
		add_to_array = { provinces_array = 1599 }
		add_to_array = { provinces_array = 7023 }
		buildings = {
			infrastructure = 4
		}
	}
}