state = {
	id = 672
	name = "STATE_672" #Mila-Nipal Camp
	provinces = { 10349 }
	manpower = 1898
	state_category = state_level_1
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = MNP
		add_core_of = MNP
		victory_points = { 10349 1 } #Aidanat Camp
		add_to_array = { provinces_array = 10349 }
		buildings = {
			infrastructure = 3
		}
	}
}