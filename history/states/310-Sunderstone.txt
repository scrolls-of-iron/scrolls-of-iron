state = {
	id = 310
	name = "STATE_310" #Sunderstone
	provinces = { 3419 }
	manpower = 7686
	state_category = state_level_3
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = FAL
		add_core_of = FAL
		add_core_of = ILI
		add_to_array = { provinces_array = 3419 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 81 index = 2 } #Nord
		add_to_array = { array = demographics_array_race value = 19 index = 5 } #Reachman
		buildings = {
			infrastructure = 3
		}
	}
}