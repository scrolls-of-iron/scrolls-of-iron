state = {
	id = 426
	name = "STATE_426" #Halted Stream
	provinces = { 1139 3297 3500 5729 6002 7255 9698 }
	manpower = 12700
	state_category = state_level_4
	resources = {
		resource_iron = 21
		resource_corundum = 2
	}
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = WHI
		add_core_of = WHI
		add_to_array = { provinces_array = 1139 }
		add_to_array = { provinces_array = 3297 }
		add_to_array = { provinces_array = 3500 }
		add_to_array = { provinces_array = 5729 }
		add_to_array = { provinces_array = 6002 }
		add_to_array = { provinces_array = 7255 }
		add_to_array = { provinces_array = 9698 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 100 index = 2 } #Nord
		buildings = {
			infrastructure = 5
		}
	}
}