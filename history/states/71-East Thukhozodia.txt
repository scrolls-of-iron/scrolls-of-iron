state = {
	id = 71
	name = "STATE_71" #East Thukhozodia
	provinces = { 534 }
	manpower = 5501
	state_category = state_level_3
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = SHM
		add_core_of = SHM
		add_core_of = BLA
		add_to_array = { provinces_array = 534 }
		buildings = {
			infrastructure = 3
		}
	}
}