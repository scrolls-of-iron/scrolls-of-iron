state = {
	id = 520
	name = "STATE_520" #Fellhammer
	provinces = { 1172 5490 7061 10928 10997 11082 }
	manpower = 10917
	state_category = state_level_4
	resources = {
		resource_iron = 19
	}
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = PAL
		add_core_of = PAL
		victory_points = { 10997 5 } #Fellhammer
		add_to_array = { provinces_array = 1172 }
		add_to_array = { provinces_array = 5490 }
		add_to_array = { provinces_array = 7061 }
		add_to_array = { provinces_array = 10928 }
		add_to_array = { provinces_array = 10997 }
		add_to_array = { provinces_array = 11082 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 100 index = 2 } #Nord
		buildings = {
			infrastructure = 4
		}
	}
}