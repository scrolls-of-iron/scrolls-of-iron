state = {
	id = 659
	name = "STATE_659" #Shara
	provinces = { 3472 6516 6804 7009 7109 9946 }
	manpower = 3423
	state_category = state_level_0
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = AAA
		victory_points = { 6516 1 } #Shara
		add_to_array = { provinces_array = 3472 }
		add_to_array = { provinces_array = 6516 }
		add_to_array = { provinces_array = 6804 }
		add_to_array = { provinces_array = 7009 }
		add_to_array = { provinces_array = 7109 }
		add_to_array = { provinces_array = 9946 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 38 index = 2 } #Nord
		add_to_array = { array = demographics_array_race value = 62 index = 8 } #Dunmer
	}
}