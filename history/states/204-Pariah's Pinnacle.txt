state = {
	id = 204
	name = "STATE_204" #Pariah's Pinnacle
	provinces = { 9734 9871 9874 }
	manpower = 17893
	state_category = state_level_4
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = JEH
		add_core_of = JEH
		add_to_array = { provinces_array = 9734 }
		add_to_array = { provinces_array = 9871 }
		add_to_array = { provinces_array = 9874 }
		buildings = {
			infrastructure = 4
		}
	}
}