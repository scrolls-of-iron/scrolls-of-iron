state = {
	id = 128
	name = "STATE_128" #South Vanneland
	provinces = { 2593 6699 7391 7392 }
	manpower = 16601
	state_category = state_level_4
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = WAY
		add_core_of = WAY
		add_core_of = ALC
		add_to_array = { provinces_array = 2593 }
		add_to_array = { provinces_array = 6699 }
		add_to_array = { provinces_array = 7391 }
		add_to_array = { provinces_array = 7392 }
		buildings = {
			infrastructure = 5
		}
	}
}