state = {
	id = 88
	name = "STATE_88" #Hinault
	provinces = { 262 }
	manpower = 20411
	state_category = state_level_4
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = SHM
		add_core_of = SHM
		add_to_array = { provinces_array = 262 }
		buildings = {
			infrastructure = 4
		}
	}
}