state = {
	id = 646
	name = "STATE_646" #Varstaad
	provinces = { 562 896 4311 5468 }
	manpower = 1459
	state_category = state_level_1
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = AAA
		victory_points = { 896 1 } #Varstaad
		add_to_array = { provinces_array = 562 }
		add_to_array = { provinces_array = 896 }
		add_to_array = { provinces_array = 4311 }
		add_to_array = { provinces_array = 5468 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 100 index = 2 } #Nord
	}
}