state = {
	id = 539
	name = "STATE_539" #West Winterhold Mountains
	provinces = { 11054 }
	manpower = 775
	state_category = state_level_0
	impassable = yes
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = PAL
		add_core_of = PAL
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 100 index = 2 } #Nord
	}
}