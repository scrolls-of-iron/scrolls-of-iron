state = {
	id = 89
	name = "STATE_89" #Traitor's Tor
	provinces = { 7158 }
	manpower = 17860
	state_category = state_level_4
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = SHM
		add_core_of = SHM
		add_to_array = { provinces_array = 7158 }
		buildings = {
			infrastructure = 4
		}
	}
}