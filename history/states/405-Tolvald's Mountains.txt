state = {
	id = 405
	name = "STATE_405" #Tolvald's Mountains
	provinces = { 1576 }
	manpower = 1391
	state_category = state_level_0
	impassable = yes
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = RFT
		add_core_of = RFT
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 100 index = 2 } #Nord
	}
}