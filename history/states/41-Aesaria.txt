state = {
	id = 41
	name = "STATE_41" #Aesaria
	provinces = { 4390 4849 7204 }
	manpower = 49778
	state_category = state_level_6
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = CAM
		add_core_of = CAM
		add_core_of = DAR
		add_to_array = { provinces_array = 4390 }
		add_to_array = { provinces_array = 4849 }
		add_to_array = { provinces_array = 7204 }
		buildings = {
			infrastructure = 5
		}
	}
}