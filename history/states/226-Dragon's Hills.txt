state = {
	id = 226
	name = "STATE_226" #Dragon's Hills
	provinces = { 5396 9705 }
	manpower = 8735
	state_category = state_level_3
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = HAF
		add_core_of = HAF
		add_to_array = { provinces_array = 5396 }
		add_to_array = { provinces_array = 9705 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 61 index = 2 } #Nord
		add_to_array = { array = demographics_array_race value = 39 index = 5 } #Reachman
		buildings = {
			infrastructure = 5
		}
	}
}