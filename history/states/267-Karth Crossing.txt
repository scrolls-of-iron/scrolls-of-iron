state = {
	id = 267
	name = "STATE_267" #Karth Crossing
	provinces = { 1901 7164 10080 10127 }
	manpower = 10995
	state_category = state_level_4
	resources = {
		resource_corundum = 1
	}
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = RCH
		add_core_of = RCH
		add_core_of = KAR
		add_to_array = { provinces_array = 1901 }
		add_to_array = { provinces_array = 7164 }
		add_to_array = { provinces_array = 10080 }
		add_to_array = { provinces_array = 10127 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 34 index = 2 } #Nord
		add_to_array = { array = demographics_array_race value = 66 index = 5 } #Reachman
		buildings = {
			infrastructure = 5
		}
	}
}