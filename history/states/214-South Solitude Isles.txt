state = {
	id = 214
	name = "STATE_214" #South Solitude Isles
	provinces = { 2729 3075 6693 6946 6948 9671 9681 9691 9701 9740 9743 }
	manpower = 468
	state_category = state_level_0
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = HAF
		add_core_of = HAF
		add_to_array = { provinces_array = 2729 }
		add_to_array = { provinces_array = 3075 }
		add_to_array = { provinces_array = 6693 }
		add_to_array = { provinces_array = 6946 }
		add_to_array = { provinces_array = 6948 }
		add_to_array = { provinces_array = 9671 }
		add_to_array = { provinces_array = 9681 }
		add_to_array = { provinces_array = 9691 }
		add_to_array = { provinces_array = 9701 }
		add_to_array = { provinces_array = 9740 }
		add_to_array = { provinces_array = 9743 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 100 index = 2 } #Nord
		buildings = {
			infrastructure = 1
		}
	}
}