state = {
	id = 532
	name = "STATE_532" #Shearpoint
	provinces = { 280 11066 11112 }
	manpower = 7878
	state_category = state_level_3
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = PAL
		add_core_of = PAL
		add_core_of = YOR
		add_to_array = { provinces_array = 280 }
		add_to_array = { provinces_array = 11066 }
		add_to_array = { provinces_array = 11112 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 100 index = 2 } #Nord
		buildings = {
			infrastructure = 3
		}
	}
}