state = {
	id = 319
	name = "STATE_319" #Lady's Coast
	provinces = { 243 337 4414 4948 6081 7467 10076 10326 }
	manpower = 20752
	state_category = state_level_4
	resources = {
		resource_corundum = 1
		resource_iron = 4
	}
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = FAL
		add_core_of = FAL
		add_core_of = ILI
		add_to_array = { provinces_array = 243 }
		add_to_array = { provinces_array = 337 }
		add_to_array = { provinces_array = 4414 }
		add_to_array = { provinces_array = 4948 }
		add_to_array = { provinces_array = 6081 }
		add_to_array = { provinces_array = 7467 }
		add_to_array = { provinces_array = 10076 }
		add_to_array = { provinces_array = 10326 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 100 index = 2 } #Nord
		buildings = {
			infrastructure = 6
		}
	}
}