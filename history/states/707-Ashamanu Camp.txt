state = {
	id = 707
	name = "STATE_707" #Ashamanu Camp
	provinces = { 7344 10204 }
	manpower = 1424
	state_category = state_level_1
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = ASM
		add_core_of = ASM
		victory_points = { 10204 1 } #Ashamanu Camp
		add_to_array = { provinces_array = 7344 }
		add_to_array = { provinces_array = 10204 }
		buildings = {
			infrastructure = 1
		}
	}
}