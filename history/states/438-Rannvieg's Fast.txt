state = {
	id = 438
	name = "STATE_438" #Rannvieg's Fast
	provinces = { 1096 9780 9788 10153 }
	manpower = 11276
	state_category = state_level_4
	resources = {
		resource_iron = 2
	}
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = WHI
		add_core_of = WHI
		add_core_of = GRM
		victory_points = { 10153 1 } #Rannvieg's Fast
		add_to_array = { provinces_array = 1096 }
		add_to_array = { provinces_array = 9780 }
		add_to_array = { provinces_array = 9788 }
		add_to_array = { provinces_array = 10153 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 100 index = 2 } #Nord
		buildings = {
			infrastructure = 4
		}
	}
}