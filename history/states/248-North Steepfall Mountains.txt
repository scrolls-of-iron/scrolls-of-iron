state = {
	id = 248
	name = "STATE_248" #North Steepfall Mountains
	provinces = { 9764 }
	manpower = 418
	state_category = state_level_0
	impassable = yes
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = HAF
		add_core_of = HAF
		add_core_of = HRA
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 100 index = 2 } #Nord
	}
}