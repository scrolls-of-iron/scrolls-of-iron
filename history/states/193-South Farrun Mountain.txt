state = {
	id = 193
	name = "STATE_193" #South Farrun Mountain
	provinces = { 1408 }
	manpower = 2868
	state_category = state_level_0
	impassable = yes
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = FAR
		add_core_of = FAR
		add_core_of = KGN
	}
}