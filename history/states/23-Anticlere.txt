state = {
	id = 23
	name = "STATE_23" #Anticlere
	provinces = { 2828 2907 5960 7567 10416 10418 10423 10439 10441 10444 10456 }
	manpower = 143532
	state_category = state_level_9
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = WAY
		add_core_of = WAY
		add_core_of = ATC
		victory_points = { 7567 10 } #Anticlere
		add_to_array = { provinces_array = 2828 }
		add_to_array = { provinces_array = 2907 }
		add_to_array = { provinces_array = 5960 }
		add_to_array = { provinces_array = 7567 }
		add_to_array = { provinces_array = 10416 }
		add_to_array = { provinces_array = 10418 }
		add_to_array = { provinces_array = 10423 }
		add_to_array = { provinces_array = 10439 }
		add_to_array = { provinces_array = 10441 }
		add_to_array = { provinces_array = 10444 }
		add_to_array = { provinces_array = 10456 }
		buildings = {
			infrastructure = 7
		}
	}
}