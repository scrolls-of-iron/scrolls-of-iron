state = {
	id = 502
	name = "STATE_502" #North-Central Labyrinthine Mountains
	provinces = { 10262 }
	manpower = 1855
	state_category = state_level_0
	impassable = yes
	resources = {
		resource_orichalcum = 3
		resource_ebony = 2
		resource_moonstone = 1
	}
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = HJL
		add_core_of = HJL
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 100 index = 2 } #Nord
	}
}