state = {
	id = 611
	name = "STATE_611" #Darkwater Crossing
	provinces = { 228 265 1296 2940 3716 4909 5274 5841 6357 7301 7317 7337 10166 10185 10187 10188 10193 10194 10195 10212 }
	manpower = 32694
	state_category = state_level_5
	resources = {
		resource_corundum = 18
		resource_iron = 10
		resource_orichalcum = 1
	}
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = EAM
		add_core_of = EAM
		add_core_of = DKW
		victory_points = { 4909 10 } #Darkwater Crossing
		victory_points = { 10193 3 } #Mistwatch
		add_to_array = { provinces_array = 228 }
		add_to_array = { provinces_array = 265 }
		add_to_array = { provinces_array = 1296 }
		add_to_array = { provinces_array = 2940 }
		add_to_array = { provinces_array = 3716 }
		add_to_array = { provinces_array = 4909 }
		add_to_array = { provinces_array = 5274 }
		add_to_array = { provinces_array = 5841 }
		add_to_array = { provinces_array = 6357 }
		add_to_array = { provinces_array = 7301 }
		add_to_array = { provinces_array = 7317 }
		add_to_array = { provinces_array = 7337 }
		add_to_array = { provinces_array = 10166 }
		add_to_array = { provinces_array = 10185 }
		add_to_array = { provinces_array = 10187 }
		add_to_array = { provinces_array = 10188 }
		add_to_array = { provinces_array = 10193 }
		add_to_array = { provinces_array = 10194 }
		add_to_array = { provinces_array = 10195 }
		add_to_array = { provinces_array = 10212 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 100 index = 2 } #Nord
		buildings = {
			infrastructure = 4
		}
	}
}