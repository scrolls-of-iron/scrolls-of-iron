state = {
	id = 37
	name = "STATE_37" #Balefire
	provinces = { 975 5029 5337 6248 6273 10218 10220 10226 10233 10243 10318 }
	manpower = 72595
	state_category = state_level_7
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = CAM
		add_core_of = CAM
		add_to_array = { provinces_array = 975 }
		add_to_array = { provinces_array = 5029 }
		add_to_array = { provinces_array = 5337 }
		add_to_array = { provinces_array = 6248 }
		add_to_array = { provinces_array = 6273 }
		add_to_array = { provinces_array = 10218 }
		add_to_array = { provinces_array = 10220 }
		add_to_array = { provinces_array = 10226 }
		add_to_array = { provinces_array = 10233 }
		add_to_array = { provinces_array = 10243 }
		add_to_array = { provinces_array = 10318 }
		buildings = {
			infrastructure = 7
		}
	}
}