state = {
	id = 241
	name = "STATE_241" #Widow's Watch
	provinces = { 3060 3675 6806 6947 9677 9716 9750 }
	manpower = 7314
	state_category = state_level_3
	resources = {
		resource_iron = 1
	}
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = HAF
		add_core_of = HAF
		add_core_of = HRA
		add_to_array = { provinces_array = 3060 }
		add_to_array = { provinces_array = 3675 }
		add_to_array = { provinces_array = 6806 }
		add_to_array = { provinces_array = 6947 }
		add_to_array = { provinces_array = 9677 }
		add_to_array = { provinces_array = 9716 }
		add_to_array = { provinces_array = 9750 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 100 index = 2 } #Nord
		buildings = {
			infrastructure = 5
		}
	}
}