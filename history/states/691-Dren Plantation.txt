state = {
	id = 691
	name = "STATE_691" #Dren Plantation
	provinces = { 1950 2407 4806 6260 }
	manpower = 26845
	state_category = state_level_5
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = HLA
		add_core_of = HLA
		add_to_array = { provinces_array = 1950 }
		add_to_array = { provinces_array = 2407 }
		add_to_array = { provinces_array = 4806 }
		add_to_array = { provinces_array = 6260 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 1 index = 0 } #Colovian
		add_to_array = { array = demographics_array_race value = 3 index = 1 } #Nibenese
		add_to_array = { array = demographics_array_race value = 2 index = 2 } #Nord
		add_to_array = { array = demographics_array_race value = 1 index = 4 } #Breton
		add_to_array = { array = demographics_array_race value = 81 index = 8 } #Dunmer
		add_to_array = { array = demographics_array_race value = 3 index = 9 } #Orsimer
		add_to_array = { array = demographics_array_race value = 5 index = 10 } #Argonian
		add_to_array = { array = demographics_array_race value = 4 index = 11 } #Khajiit
		buildings = {
			infrastructure = 6
		}
	}
}