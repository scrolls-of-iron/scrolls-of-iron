state = {
	id = 481
	name = "STATE_481" #Snowhawk
	provinces = { 1510 7052 7054 10572 }
	manpower = 12191
	state_category = state_level_4
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = HJL
		add_core_of = HJL
		add_core_of = SNO
		victory_points = { 10572 10 } #Snowhawk
		add_to_array = { provinces_array = 1510 }
		add_to_array = { provinces_array = 7052 }
		add_to_array = { provinces_array = 7054 }
		add_to_array = { provinces_array = 10572 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 100 index = 2 } #Nord
		buildings = {
			infrastructure = 5
			10572 = {
				naval_base = 2
			}
		}
	}
}