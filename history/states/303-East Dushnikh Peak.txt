state = {
	id = 303
	name = "STATE_303" #East Dushnikh Peak
	provinces = { 2530 }
	manpower = 392
	state_category = state_level_0
	impassable = yes
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = RCH
		add_core_of = RCH
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 44 index = 5 } #Reachman
		add_to_array = { array = demographics_array_race value = 56 index = 9 } #Orsimer
	}
}