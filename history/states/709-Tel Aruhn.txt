state = {
	id = 709
	name = "STATE_709" #Tel Aruhn
	provinces = { 1451 2943 5138 6683 7303 7310 7340 7348 7358 7359 10208 10216 }
	manpower = 19939
	state_category = state_level_4
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = TEL
		add_core_of = TEL
		victory_points = { 7359 10 } #Tel Aruhn
		add_to_array = { provinces_array = 1451 }
		add_to_array = { provinces_array = 2943 }
		add_to_array = { provinces_array = 5138 }
		add_to_array = { provinces_array = 6683 }
		add_to_array = { provinces_array = 7303 }
		add_to_array = { provinces_array = 7310 }
		add_to_array = { provinces_array = 7340 }
		add_to_array = { provinces_array = 7348 }
		add_to_array = { provinces_array = 7358 }
		add_to_array = { provinces_array = 7359 }
		add_to_array = { provinces_array = 10208 }
		add_to_array = { provinces_array = 10216 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 2 index = 0 } #Colovian
		add_to_array = { array = demographics_array_race value = 3 index = 1 } #Nibenese
		add_to_array = { array = demographics_array_race value = 7 index = 3 } #Redguard
		add_to_array = { array = demographics_array_race value = 5 index = 4 } #Breton
		add_to_array = { array = demographics_array_race value = 2 index = 6 } #Altmer
		add_to_array = { array = demographics_array_race value = 5 index = 7 } #Bosmer
		add_to_array = { array = demographics_array_race value = 64 index = 8 } #Dunmer
		add_to_array = { array = demographics_array_race value = 10 index = 9 } #Orsimer
		add_to_array = { array = demographics_array_race value = 2 index = 11 } #Khajiit
		buildings = {
			infrastructure = 4
		}
	}
}