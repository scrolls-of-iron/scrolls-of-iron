state = {
	id = 315
	name = "STATE_315" #Embershard
	provinces = { 1552 3351 4803 7420 }
	manpower = 13834
	state_category = state_level_4
	resources = {
		resource_iron = 15
	}
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = FAL
		add_core_of = FAL
		add_core_of = ILI
		victory_points = { 1552 1 } #Embershard
		add_to_array = { provinces_array = 1552 }
		add_to_array = { provinces_array = 3351 }
		add_to_array = { provinces_array = 4803 }
		add_to_array = { provinces_array = 7420 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 100 index = 2 } #Nord
		buildings = {
			infrastructure = 4
		}
	}
}