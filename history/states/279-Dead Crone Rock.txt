state = {
	id = 279
	name = "STATE_279" #Dead Crone Rock
	provinces = { 1192 2076 7308 10077 10083 10085 }
	manpower = 15993
	state_category = state_level_4
	resources = {
		resource_iron = 3
		resource_quicksilver = 2
	}
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = RCH
		add_core_of = RCH
		add_to_array = { provinces_array = 1192 }
		add_to_array = { provinces_array = 2076 }
		add_to_array = { provinces_array = 7308 }
		add_to_array = { provinces_array = 10077 }
		add_to_array = { provinces_array = 10083 }
		add_to_array = { provinces_array = 10085 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 21 index = 2 } #Nord
		add_to_array = { array = demographics_array_race value = 70 index = 5 } #Reachman
		add_to_array = { array = demographics_array_race value = 9 index = 9 } #Orsimer
		buildings = {
			infrastructure = 3
		}
	}
}