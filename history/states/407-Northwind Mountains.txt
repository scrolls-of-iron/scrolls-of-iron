state = {
	id = 407
	name = "STATE_407" #Northwind Mountains
	provinces = { 1827 }
	manpower = 2568
	state_category = state_level_0
	impassable = yes
	resources = {
		resource_moonstone = 1
	}
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = RFT
		add_core_of = RFT
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 100 index = 2 } #Nord
	}
}