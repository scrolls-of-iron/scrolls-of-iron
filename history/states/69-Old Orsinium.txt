state = {
	id = 69
	name = "STATE_69" #Old Orsinium
	provinces = { 2440 6396 7210 7221 7246 9935 }
	manpower = 18338
	state_category = state_level_4
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = SHM
		add_core_of = SHM
		add_core_of = BLA
		add_to_array = { provinces_array = 2440 }
		add_to_array = { provinces_array = 6396 }
		add_to_array = { provinces_array = 7210 }
		add_to_array = { provinces_array = 7221 }
		add_to_array = { provinces_array = 7246 }
		add_to_array = { provinces_array = 9935 }
		buildings = {
			infrastructure = 3
		}
	}
}