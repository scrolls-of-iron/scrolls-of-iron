state = {
	id = 108
	name = "STATE_108" #Western Orsinium
	provinces = { 2609 5367 }
	manpower = 15007
	state_category = state_level_4
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = ORS
		add_core_of = ORS
		add_to_array = { provinces_array = 2609 }
		add_to_array = { provinces_array = 5367 }
		buildings = {
			infrastructure = 4
		}
	}
}