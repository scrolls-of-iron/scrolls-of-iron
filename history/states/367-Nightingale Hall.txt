state = {
	id = 367
	name = "STATE_367" #Nightingale Hall
	provinces = { 2275 10467 }
	manpower = 2140
	state_category = state_level_1
	resources = {
		resource_iron = 3
	}
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = RFT
		add_core_of = RFT
		add_to_array = { provinces_array = 2275 }
		add_to_array = { provinces_array = 10467 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 100 index = 2 } #Nord
		buildings = {
			infrastructure = 6
		}
	}
}