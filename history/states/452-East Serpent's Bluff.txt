state = {
	id = 452
	name = "STATE_452" #East Serpent's Bluff
	provinces = { 1545 9697 9714 10392 }
	manpower = 10047
	state_category = state_level_4
	resources = {
		resource_iron = 1
	}
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = WHI
		add_core_of = WHI
		add_core_of = ROR
		add_to_array = { provinces_array = 1545 }
		add_to_array = { provinces_array = 9697 }
		add_to_array = { provinces_array = 9714 }
		add_to_array = { provinces_array = 10392 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 52 index = 2 } #Nord
		add_to_array = { array = demographics_array_race value = 48 index = 5 } #Reachman
		buildings = {
			infrastructure = 3
		}
	}
}