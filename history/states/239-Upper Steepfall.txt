state = {
	id = 239
	name = "STATE_239" #Upper Steepfall
	provinces = { 6372 }
	manpower = 4179
	state_category = state_level_2
	resources = {
		resource_corundum = 2
	}
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = HAF
		add_core_of = HAF
		add_core_of = HRA
		add_to_array = { provinces_array = 6372 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 100 index = 2 } #Nord
		buildings = {
			infrastructure = 5
		}
	}
}