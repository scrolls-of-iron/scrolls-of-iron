state = {
	id = 658
	name = "STATE_658" #Odirnamat
	provinces = { 1299 1849 2596 6677 7060 9964 9973 }
	manpower = 4075
	state_category = state_level_0
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = AAA
		victory_points = { 6677 1 } #Odirnamat
		add_to_array = { provinces_array = 1299 }
		add_to_array = { provinces_array = 1849 }
		add_to_array = { provinces_array = 2596 }
		add_to_array = { provinces_array = 6677 }
		add_to_array = { provinces_array = 7060 }
		add_to_array = { provinces_array = 9964 }
		add_to_array = { provinces_array = 9973 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 56 index = 2 } #Nord
		add_to_array = { array = demographics_array_race value = 44 index = 8 } #Dunmer
	}
}