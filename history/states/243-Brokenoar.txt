state = {
	id = 243
	name = "STATE_243" #Brokenoar
	provinces = { 2707 5077 5951 9687 9690 9704 9706 9713 9718 9783 }
	manpower = 17135
	state_category = state_level_4
	resources = {
		resource_corundum = 1
		resource_iron = 1
		resource_moonstone = 1
	}
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = HAF
		add_core_of = HAF
		add_to_array = { provinces_array = 2707 }
		add_to_array = { provinces_array = 5077 }
		add_to_array = { provinces_array = 5951 }
		add_to_array = { provinces_array = 9687 }
		add_to_array = { provinces_array = 9690 }
		add_to_array = { provinces_array = 9704 }
		add_to_array = { provinces_array = 9706 }
		add_to_array = { provinces_array = 9713 }
		add_to_array = { provinces_array = 9718 }
		add_to_array = { provinces_array = 9783 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 100 index = 2 } #Nord
		buildings = {
			infrastructure = 5
		}
	}
}