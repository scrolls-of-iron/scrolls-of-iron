state = {
	id = 137
	name = "STATE_137" #Windkeep
	provinces = { 656 7364 7379 10270 10301 10302 10332 }
	manpower = 62355
	state_category = state_level_6
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = WAY
		add_core_of = WAY
		add_core_of = GAV
		add_to_array = { provinces_array = 656 }
		add_to_array = { provinces_array = 7364 }
		add_to_array = { provinces_array = 7379 }
		add_to_array = { provinces_array = 10270 }
		add_to_array = { provinces_array = 10301 }
		add_to_array = { provinces_array = 10302 }
		add_to_array = { provinces_array = 10332 }
		buildings = {
			infrastructure = 6
		}
	}
}