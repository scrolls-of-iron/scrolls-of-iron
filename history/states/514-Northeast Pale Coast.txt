state = {
	id = 514
	name = "STATE_514" #Northeast Pale Coast
	provinces = { 4469 6972 10909 }
	manpower = 3211
	state_category = state_level_2
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = PAL
		add_core_of = PAL
		add_to_array = { provinces_array = 4469 }
		add_to_array = { provinces_array = 6972 }
		add_to_array = { provinces_array = 10909 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 100 index = 2 } #Nord
		buildings = {
			infrastructure = 3
		}
	}
}