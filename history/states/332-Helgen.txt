state = {
	id = 332
	name = "STATE_332" #Helgen
	provinces = { 9763 9837 10341 }
	manpower = 17260
	state_category = state_level_4
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = FAL
		add_core_of = FAL
		add_core_of = HGN
		victory_points = { 9763 15 } #Helgen
		add_to_array = { provinces_array = 9763 }
		add_to_array = { provinces_array = 9837 }
		add_to_array = { provinces_array = 10341 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 100 index = 2 } #Nord
		buildings = {
			infrastructure = 6
		}
	}
}