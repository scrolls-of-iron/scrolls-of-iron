state = {
	id = 577
	name = "STATE_577" #Coast of Winterhold
	provinces = { 6938 }
	manpower = 5986
	state_category = state_level_3
	resources = {
		resource_iron = 1
	}
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = WNT
		add_core_of = WNT
		add_to_array = { provinces_array = 6938 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 100 index = 2 } #Nord
		buildings = {
			infrastructure = 1
		}
	}
}