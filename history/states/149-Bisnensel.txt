state = {
	id = 149
	name = "STATE_149" #Bisnensel
	provinces = { 2044 5345 }
	manpower = 34285
	state_category = state_level_5
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = EVR
		add_core_of = EVR
		add_to_array = { provinces_array = 2044 }
		add_to_array = { provinces_array = 5345 }
		buildings = {
			infrastructure = 6
		}
	}
}