state = {
	id = 29
	name = "STATE_29" #Northern Dwynnen
	provinces = { 2036 3237 7396 7403 10230 }
	manpower = 63016
	state_category = state_level_6
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = DAG
		add_core_of = DAG
		add_core_of = DWY
		victory_points = { 7396 1 } #Whitewold
		add_to_array = { provinces_array = 2036 }
		add_to_array = { provinces_array = 3237 }
		add_to_array = { provinces_array = 7396 }
		add_to_array = { provinces_array = 7403 }
		add_to_array = { provinces_array = 10230 }
		buildings = {
			infrastructure = 5
		}
	}
}