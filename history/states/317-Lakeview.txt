state = {
	id = 317
	name = "STATE_317" #Lakeview
	provinces = { 303 1335 4281 }
	manpower = 18446
	state_category = state_level_4
	resources = {
		resource_iron = 3
	}
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = FAL
		add_core_of = FAL
		add_core_of = ILI
		victory_points = { 303 3 } #Lakeview
		add_to_array = { provinces_array = 303 }
		add_to_array = { provinces_array = 1335 }
		add_to_array = { provinces_array = 4281 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 100 index = 2 } #Nord
		buildings = {
			infrastructure = 5
		}
	}
}