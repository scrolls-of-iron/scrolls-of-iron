state = {
	id = 151
	name = "STATE_151" #North Halcyonia
	provinces = { 1707 3458 4439 }
	manpower = 18571
	state_category = state_level_4
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = EVR
		add_core_of = EVR
		add_core_of = RAV
		add_to_array = { provinces_array = 1707 }
		add_to_array = { provinces_array = 3458 }
		add_to_array = { provinces_array = 4439 }
		buildings = {
			infrastructure = 4
		}
	}
}