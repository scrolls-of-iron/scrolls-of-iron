state = {
	id = 205
	name = "STATE_205" #Jehanna
	provinces = { 4393 9820 9831 }
	manpower = 63767
	state_category = state_level_6
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = JEH
		add_core_of = JEH
		victory_points = { 4393 15 } #Jehanna
		victory_points = { 9820 10 } #Jehanna
		add_to_array = { provinces_array = 4393 }
		add_to_array = { provinces_array = 9820 }
		add_to_array = { provinces_array = 9831 }
		buildings = {
			infrastructure = 7
		}
	}
}