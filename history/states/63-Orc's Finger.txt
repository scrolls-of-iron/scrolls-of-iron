state = {
	id = 63
	name = "STATE_63" #Orc's Finger
	provinces = { 7083 7100 9808 9830 9934 9958 9966 9970 9971 }
	manpower = 17319
	state_category = state_level_4
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = NPT
		add_core_of = NPT
		add_core_of = NMR
		add_to_array = { provinces_array = 7083 }
		add_to_array = { provinces_array = 7100 }
		add_to_array = { provinces_array = 9808 }
		add_to_array = { provinces_array = 9830 }
		add_to_array = { provinces_array = 9934 }
		add_to_array = { provinces_array = 9958 }
		add_to_array = { provinces_array = 9966 }
		add_to_array = { provinces_array = 9970 }
		add_to_array = { provinces_array = 9971 }
		buildings = {
			infrastructure = 3
		}
	}
}