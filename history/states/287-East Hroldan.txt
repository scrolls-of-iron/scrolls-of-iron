state = {
	id = 287
	name = "STATE_287" #East Hroldan
	provinces = { 5558 7285 9903 9936 9949 9967 10164 10176 }
	manpower = 9236
	state_category = state_level_3
	resources = {
		resource_iron = 3
	}
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = RCH
		add_core_of = RCH
		add_core_of = SGD
		add_to_array = { provinces_array = 5558 }
		add_to_array = { provinces_array = 7285 }
		add_to_array = { provinces_array = 9903 }
		add_to_array = { provinces_array = 9936 }
		add_to_array = { provinces_array = 9949 }
		add_to_array = { provinces_array = 9967 }
		add_to_array = { provinces_array = 10164 }
		add_to_array = { provinces_array = 10176 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 51 index = 2 } #Nord
		add_to_array = { array = demographics_array_race value = 49 index = 5 } #Reachman
		buildings = {
			infrastructure = 4
		}
	}
}