state = {
	id = 46
	name = "STATE_46" #Bedraud
	provinces = { 650 3645 7314 7326 10201 }
	manpower = 28668
	state_category = state_level_5
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = CAM
		add_core_of = CAM
		add_core_of = EAG
		add_to_array = { provinces_array = 650 }
		add_to_array = { provinces_array = 3645 }
		add_to_array = { provinces_array = 7314 }
		add_to_array = { provinces_array = 7326 }
		add_to_array = { provinces_array = 10201 }
		buildings = {
			infrastructure = 4
		}
	}
}