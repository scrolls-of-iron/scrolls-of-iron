state = {
	id = 44
	name = "STATE_44" #Par Molag
	provinces = { 100 1396 4727 5500 7263 7299 7306 }
	manpower = 42874
	state_category = state_level_6
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = CAM
		add_core_of = CAM
		add_core_of = EAG
		add_to_array = { provinces_array = 100 }
		add_to_array = { provinces_array = 1396 }
		add_to_array = { provinces_array = 4727 }
		add_to_array = { provinces_array = 5500 }
		add_to_array = { provinces_array = 7263 }
		add_to_array = { provinces_array = 7299 }
		add_to_array = { provinces_array = 7306 }
		buildings = {
			infrastructure = 4
		}
	}
}