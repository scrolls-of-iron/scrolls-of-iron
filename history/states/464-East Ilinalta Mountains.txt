state = {
	id = 464
	name = "STATE_464" #North Hrothgar Mountains
	provinces = { 113 }
	manpower = 896
	state_category = state_level_0
	impassable = yes
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = WHI
		add_core_of = WHI
		add_core_of = RVW
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 100 index = 2 } #Nord
	}
}