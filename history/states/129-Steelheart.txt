state = {
	id = 129
	name = "STATE_129" #Steelheart
	provinces = { 1208 6139 7367 7394 }
	manpower = 19279
	state_category = state_level_4
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = WAY
		add_core_of = WAY
		add_core_of = ALC
		add_to_array = { provinces_array = 1208 }
		add_to_array = { provinces_array = 6139 }
		add_to_array = { provinces_array = 7367 }
		add_to_array = { provinces_array = 7394 }
		buildings = {
			infrastructure = 5
		}
	}
}