state = {
	id = 358
	name = "STATE_358" #Nimalten
	provinces = { 2716 3902 4411 7484 10489 }
	manpower = 26804
	state_category = state_level_5
	resources = {
		resource_iron = 4
		resource_corundum = 1
	}
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = RFT
		add_core_of = RFT
		add_core_of = NIM
		victory_points = { 3902 10 } #Nimalten
		add_to_array = { provinces_array = 2716 }
		add_to_array = { provinces_array = 3902 }
		add_to_array = { provinces_array = 4411 }
		add_to_array = { provinces_array = 7484 }
		add_to_array = { provinces_array = 10489 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 100 index = 2 } #Nord
		buildings = {
			infrastructure = 5
		}
	}
}