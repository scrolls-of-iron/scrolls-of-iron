state = {
	id = 684
	name = "STATE_684" #Hla Oad
	provinces = { 62 4623 7534 7546 7574 }
	manpower = 16266
	state_category = state_level_4
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = HLA
		add_core_of = HLA
		add_to_array = { provinces_array = 62 }
		add_to_array = { provinces_array = 4623 }
		add_to_array = { provinces_array = 7534 }
		add_to_array = { provinces_array = 7546 }
		add_to_array = { provinces_array = 7574 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 1 index = 0 } #Colovian
		add_to_array = { array = demographics_array_race value = 5 index = 1 } #Nibenese
		add_to_array = { array = demographics_array_race value = 3 index = 2 } #Nord
		add_to_array = { array = demographics_array_race value = 3 index = 3 } #Redguard
		add_to_array = { array = demographics_array_race value = 3 index = 7 } #Bosmer
		add_to_array = { array = demographics_array_race value = 73 index = 8 } #Dunmer
		add_to_array = { array = demographics_array_race value = 3 index = 9 } #Orsimer
		add_to_array = { array = demographics_array_race value = 3 index = 10 } #Argonian
		add_to_array = { array = demographics_array_race value = 6 index = 11 } #Khajiit
		buildings = {
			infrastructure = 4
		}
	}
}