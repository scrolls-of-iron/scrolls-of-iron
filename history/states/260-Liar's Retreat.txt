state = {
	id = 260
	name = "STATE_260" #Liar's Retreat
	provinces = { 1126 10038 }
	manpower = 13994
	state_category = state_level_4
	resources = {
		resource_corundum = 4
		resource_iron = 1
	}
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = RCH
		add_core_of = RCH
		add_core_of = KAR
		add_to_array = { provinces_array = 1126 }
		add_to_array = { provinces_array = 10038 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 36 index = 2 } #Nord
		add_to_array = { array = demographics_array_race value = 64 index = 5 } #Reachman
		buildings = {
			infrastructure = 3
		}
	}
}