state = {
	id = 623
	name = "STATE_623" #Southeast Kastav Mountains
	provinces = { 9854 }
	manpower = 3027
	state_category = state_level_0
	impassable = yes
	resources = {
		resource_corundum = 2
		resource_iron = 4
		resource_moonstone = 1
	}
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = EAM
		add_core_of = EAM
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 100 index = 2 } #Nord
	}
}