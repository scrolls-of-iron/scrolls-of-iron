state = {
	id = 682
	name = "STATE_682" #Balmora
	provinces = { 385 7488 7562 10536 10565 10591 10609 10616 10626 }
	manpower = 60906
	state_category = state_level_6
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = HLA
		add_core_of = HLA
		add_to_array = { provinces_array = 385 }
		add_to_array = { provinces_array = 7488 }
		add_to_array = { provinces_array = 7562 }
		add_to_array = { provinces_array = 10536 }
		add_to_array = { provinces_array = 10565 }
		add_to_array = { provinces_array = 10591 }
		add_to_array = { provinces_array = 10609 }
		add_to_array = { provinces_array = 10616 }
		add_to_array = { provinces_array = 10626 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 5 index = 0 } #Colovian
		add_to_array = { array = demographics_array_race value = 5 index = 1 } #Nibenese
		add_to_array = { array = demographics_array_race value = 5 index = 2 } #Nord
		add_to_array = { array = demographics_array_race value = 6 index = 3 } #Redguard
		add_to_array = { array = demographics_array_race value = 5 index = 4 } #Breton
		add_to_array = { array = demographics_array_race value = 5 index = 6 } #Altmer
		add_to_array = { array = demographics_array_race value = 3 index = 7 } #Bosmer
		add_to_array = { array = demographics_array_race value = 56 index = 8 } #Dunmer
		add_to_array = { array = demographics_array_race value = 2 index = 9 } #Orsimer
		add_to_array = { array = demographics_array_race value = 4 index = 10 } #Argonian
		add_to_array = { array = demographics_array_race value = 4 index = 11 } #Khajiit
		buildings = {
			infrastructure = 6
		}
	}
}