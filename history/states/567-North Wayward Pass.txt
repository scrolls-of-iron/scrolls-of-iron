state = {
	id = 567
	name = "STATE_567" #North Wayward Pass
	provinces = { 9675 }
	manpower = 4695
	state_category = state_level_2
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = WNT
		add_core_of = WNT
		add_core_of = SAR
		add_to_array = { provinces_array = 9675 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 100 index = 2 } #Nord
		buildings = {
			infrastructure = 1
		}
	}
}