state = {
	id = 429
	name = "STATE_429" #Bleakwind Basin
	provinces = { 46 4558 6120 9773 9825 9996 }
	manpower = 15390
	state_category = state_level_4
	resources = {
		resource_corundum = 2
	}
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = WHI
		add_core_of = WHI
		add_to_array = { provinces_array = 46 }
		add_to_array = { provinces_array = 4558 }
		add_to_array = { provinces_array = 6120 }
		add_to_array = { provinces_array = 9773 }
		add_to_array = { provinces_array = 9825 }
		add_to_array = { provinces_array = 9996 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 100 index = 2 } #Nord
		buildings = {
			infrastructure = 6
		}
	}
}