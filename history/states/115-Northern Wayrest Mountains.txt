state = {
	id = 115
	name = "STATE_115" #Northern Wayrest Mountains
	provinces = { 10368 }
	manpower = 550
	state_category = state_level_0
	impassable = yes
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = ORS
		add_core_of = ORS
	}
}