state = {
	id = 408
	name = "STATE_408" #Clearspring Mountains
	provinces = { 453 }
	manpower = 1982
	state_category = state_level_0
	impassable = yes
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = RFT
		add_core_of = RFT
		add_core_of = IVA
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 100 index = 2 } #Nord
	}
}