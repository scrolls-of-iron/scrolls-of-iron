state = {
	id = 478
	name = "STATE_478" #Forebears' Holdout
	provinces = { 1310 2589 4246 10682 }
	manpower = 7690
	state_category = state_level_3
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = HJL
		add_core_of = HJL
		add_core_of = SNO
		add_to_array = { provinces_array = 1310 }
		add_to_array = { provinces_array = 2589 }
		add_to_array = { provinces_array = 4246 }
		add_to_array = { provinces_array = 10682 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 100 index = 2 } #Nord
		buildings = {
			infrastructure = 3
		}
	}
}