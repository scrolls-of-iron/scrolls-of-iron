state = {
	id = 428
	name = "STATE_428" #Pelagia
	provinces = { 60 7300 10030 10054 10566 }
	manpower = 20171
	state_category = state_level_4
	resources = {
		resource_iron = 2
		resource_corundum = 1
	}
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = WHI
		add_core_of = WHI
		victory_points = { 10054 3 } #Pelagia
		add_to_array = { provinces_array = 60 }
		add_to_array = { provinces_array = 7300 }
		add_to_array = { provinces_array = 10030 }
		add_to_array = { provinces_array = 10054 }
		add_to_array = { provinces_array = 10566 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 100 index = 2 } #Nord
		buildings = {
			infrastructure = 6
		}
	}
}