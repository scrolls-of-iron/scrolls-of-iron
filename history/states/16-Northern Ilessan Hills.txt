state = {
	id = 16
	name = "STATE_16" #Northern Ilessan Hills
	provinces = { 660 2803 6237 7579 }
	manpower = 96359
	state_category = state_level_8
	resources = {
		resource_food = 10
	}
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = DAG
		add_core_of = DAG
		add_core_of = ILH
		victory_points = { 660 1 } #Baelwall
		add_to_array = { provinces_array = 660 }
		add_to_array = { provinces_array = 2803 }
		add_to_array = { provinces_array = 6237 }
		add_to_array = { provinces_array = 7579 }
		buildings = {
			infrastructure = 5
		}
	}
}