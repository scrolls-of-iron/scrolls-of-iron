state = {
	id = 674
	name = "STATE_674" #Ald'ruhn
	provinces = { 5676 7315 7333 10092 10272 10474 10484 }
	manpower = 71014
	state_category = state_level_0
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = RED
		add_core_of = RED
		victory_points = { 10272 35 } #Ald'ruhn
		add_to_array = { provinces_array = 5676 }
		add_to_array = { provinces_array = 7315 }
		add_to_array = { provinces_array = 7333 }
		add_to_array = { provinces_array = 10092 }
		add_to_array = { provinces_array = 10272 }
		add_to_array = { provinces_array = 10474 }
		add_to_array = { provinces_array = 10484 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 3 index = 1 } #Nibenese
		add_to_array = { array = demographics_array_race value = 2 index = 2 } #Nord
		add_to_array = { array = demographics_array_race value = 3 index = 4 } #Breton
		add_to_array = { array = demographics_array_race value = 3 index = 6 } #Altmer
		add_to_array = { array = demographics_array_race value = 3 index = 7 } #Bosmer
		add_to_array = { array = demographics_array_race value = 84 index = 8 } #Dunmer
		add_to_array = { array = demographics_array_race value = 2 index = 10 } #Argonian
		buildings = {
			infrastructure = 4
		}
	}
}