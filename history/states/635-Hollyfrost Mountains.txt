state = {
	id = 635
	name = "STATE_635" #Hollyfrost Mountains
	provinces = { 4708 }
	manpower = 848
	state_category = state_level_0
	impassable = yes
	resources = {
		resource_corundum = 1
	}
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = EAM
		add_core_of = EAM
		add_core_of = KYN
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 100 index = 2 } #Nord
	}
}