state = {
	id = 369
	name = "STATE_369" #Crystaldrift
	provinces = { 2523 2963 }
	manpower = 11129
	state_category = state_level_4
	resources = {
		resource_corundum = 1
		resource_moonstone = 1
	}
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = RFT
		add_core_of = RFT
		add_to_array = { provinces_array = 2523 }
		add_to_array = { provinces_array = 2963 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 100 index = 2 } #Nord
		buildings = {
			infrastructure = 3
		}
	}
}