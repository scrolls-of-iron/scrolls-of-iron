state = {
	id = 543
	name = "STATE_543" #East Windward Mountain
	provinces = { 786 }
	manpower = 715
	state_category = state_level_0
	impassable = yes
	resources = {
		resource_iron = 1
	}
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = PAL
		add_core_of = PAL
		add_core_of = YOR
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 100 index = 2 } #Nord
	}
}