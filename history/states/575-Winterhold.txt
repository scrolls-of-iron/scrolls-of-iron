state = {
	id = 575
	name = "STATE_575" #Winterhold
	provinces = { 3022 9883 }
	manpower = 47238
	state_category = state_level_6
	resources = {
		resource_iron = 2
	}
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = WNT
		add_core_of = WNT
		victory_points = { 3022 30 } #Winterhold
		add_to_array = { provinces_array = 3022 }
		add_to_array = { provinces_array = 9883 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 91 index = 2 } #Nord
		add_to_array = { array = demographics_array_race value = 9 index = 8 } #Dunmer
		buildings = {
			infrastructure = 6
		}
	}
}