state = {
	id = 463
	name = "STATE_463" #North Hrothgar Mountains
	provinces = { 3683 7354 }
	manpower = 5379
	state_category = state_level_0
	impassable = yes
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = WHI
		add_core_of = WHI
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 100 index = 2 } #Nord
	}
}