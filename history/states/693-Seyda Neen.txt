state = {
	id = 693
	name = "STATE_693" #Seyda Neen
	provinces = { 2524 5631 6849 7651 10530 }
	manpower = 19278
	state_category = state_level_4
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = HLA
		add_core_of = HLA
		add_to_array = { provinces_array = 2524 }
		add_to_array = { provinces_array = 5631 }
		add_to_array = { provinces_array = 6849 }
		add_to_array = { provinces_array = 7651 }
		add_to_array = { provinces_array = 10530 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 4 index = 0 } #Colovian
		add_to_array = { array = demographics_array_race value = 8 index = 1 } #Nibenese
		add_to_array = { array = demographics_array_race value = 7 index = 2 } #Nord
		add_to_array = { array = demographics_array_race value = 2 index = 3 } #Redguard
		add_to_array = { array = demographics_array_race value = 2 index = 4 } #Breton
		add_to_array = { array = demographics_array_race value = 7 index = 6 } #Altmer
		add_to_array = { array = demographics_array_race value = 5 index = 7 } #Bosmer
		add_to_array = { array = demographics_array_race value = 60 index = 8 } #Dunmer
		add_to_array = { array = demographics_array_race value = 5 index = 10 } #Argonian
		buildings = {
			infrastructure = 5
		}
	}
}