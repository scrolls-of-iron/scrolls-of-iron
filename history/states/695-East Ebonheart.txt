state = {
	id = 695
	name = "STATE_695" #East Ebonheart
	provinces = { 10593 }
	manpower = 7214
	state_category = state_level_3
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = EEC
		add_core_of = EEC
		victory_points = { 10593 3 } #East Ebonheart
		add_to_array = { provinces_array = 10593 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 9 index = 0 } #Colovian
		add_to_array = { array = demographics_array_race value = 21 index = 1 } #Nibenese
		add_to_array = { array = demographics_array_race value = 16 index = 2 } #Nord
		add_to_array = { array = demographics_array_race value = 6 index = 3 } #Redguard
		add_to_array = { array = demographics_array_race value = 8 index = 4 } #Breton
		add_to_array = { array = demographics_array_race value = 7 index = 6 } #Altmer
		add_to_array = { array = demographics_array_race value = 5 index = 7 } #Bosmer
		add_to_array = { array = demographics_array_race value = 12 index = 8 } #Dunmer
		add_to_array = { array = demographics_array_race value = 3 index = 9 } #Orsimer
		add_to_array = { array = demographics_array_race value = 10 index = 10 } #Argonian
		add_to_array = { array = demographics_array_race value = 3 index = 11 } #Khajiit
		buildings = {
			infrastructure = 9
			10593 = { naval_base = 6 bunker = 1 }
		}
	}
}