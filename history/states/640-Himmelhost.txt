state = {
	id = 640
	name = "STATE_640" #Himmelhost
	provinces = { 1170 6632 7057 }
	manpower = 2420
	state_category = state_level_1
	history = {
		log = "Loading state history for [THIS.GetID] ([THIS.GetName])"
		owner = AAA
		victory_points = { 1170 1 } #Himmelhost
		add_to_array = { provinces_array = 1170 }
		add_to_array = { provinces_array = 6632 }
		add_to_array = { provinces_array = 7057 }
		resize_array = { demographics_array_race = 13 }
		add_to_array = { array = demographics_array_race value = 100 index = 2 } #Nord
	}
}