#Adjacency rules are special rules for adjacencies
#If somebody who is at war fights over canal/strait it is considered closed
#Enemy status is considered if any of sides controlling the strait is at war with country
#Friend status is considered non of sides controlling the strait is at war with country and one of controllers is ally or giving military access
#Everyone else is considered as neutral according to canal/strait
#Military access considered as being friend
#Rules needs linking from the straits that are affected by the rule ( adjacency_rule_id )

#Name is how you refer to the rule in adjacencies.csv
adjacency_rule = {
	name = "STRAIT_DAGGERFALL" #Strait between Tulune and Western Daggerfall
	contested = {
		army = yes
		navy = yes
		submarine = yes
		trade = yes
	}
	enemy = {
		army = no
		navy = no
		submarine = no
		trade = no
	}
	friend = {
		army = yes
		navy = yes
		submarine = yes
		trade = yes
	}
	neutral = {
		army = yes
		navy = yes
		submarine = yes
		trade = yes
	}
	is_enemy = {
		OR = {
			any_country = {
				controls_province = 5749
				not_has_strait_access = yes
			}
			any_country = {
				controls_province = 10629
				not_has_strait_access = yes
			}
			any_country = {
				controls_province = 10688
				not_has_strait_access = yes
			}
		}
		NOT = {
			AND = {
				any_country = {
					controls_province = 5749
					has_strait_access = yes
				}
				any_country = {
					controls_province = 10629
					has_strait_access = yes
				}
				any_country = {
					controls_province = 10688
					has_strait_access = yes
				}
			}
		}
	}
	required_provinces = { 5749 10629 10688 }
	icon = 10629
	offset = { 0 0 0 }
}

adjacency_rule = {
	name = "STRAIT_SOLITUDE" #Strait under Solitude
	contested = {
		army = yes
		navy = yes
		submarine = yes
		trade = yes
	}
	enemy = {
		army = no
		navy = no
		submarine = no
		trade = no
	}
	friend = {
		army = yes
		navy = yes
		submarine = yes
		trade = yes
	}
	neutral = {
		army = yes
		navy = yes
		submarine = yes
		trade = yes
	}
	is_enemy = {
		AND = {
			any_country = {
				controls_province = 6984
				OR = {
					is_in_array = { embargo_array = PREV }
					has_war_with = PREV
				}
			}
		}
	}
	required_provinces = { 6984 }
	icon = 6984
	offset = { -2 -2 0 }
}

adjacency_rule = {
	name = "STRAIT_DRAJKMYR_WEST" #Western Drajkmyr Straits
	contested = {
		army = yes
		navy = yes
		submarine = yes
		trade = yes
	}
	enemy = {
		army = no
		navy = no
		submarine = no
		trade = no
	}
	friend = {
		army = yes
		navy = yes
		submarine = yes
		trade = yes
	}
	neutral = {
		army = yes
		navy = yes
		submarine = yes
		trade = yes
	}
	is_enemy = {
		AND = {
			any_country = {
				controls_province = 1494
				OR = {
					is_in_array = { embargo_array = PREV }
					has_war_with = PREV
				}
			}
			any_country = {
				controls_province = 5378
				OR = {
					is_in_array = { embargo_array = PREV }
					has_war_with = PREV
				}
			}
			any_country = {
				controls_province = 7011
				OR = {
					is_in_array = { embargo_array = PREV }
					has_war_with = PREV
				}
			}
			any_country = {
				controls_province = 9895
				OR = {
					is_in_array = { embargo_array = PREV }
					has_war_with = PREV
				}
			}
		}
	}
	required_provinces = { 1494 5378 7011 9895 }
	icon = 1494
	offset = { -2 -2 4 }
}

adjacency_rule = {
	name = "STRAIT_DRAJKMYR_EAST" #Eastern Drajkmyr Straits
	contested = {
		army = yes
		navy = yes
		submarine = yes
		trade = yes
	}
	enemy = {
		army = no
		navy = no
		submarine = no
		trade = no
	}
	friend = {
		army = yes
		navy = yes
		submarine = yes
		trade = yes
	}
	neutral = {
		army = yes
		navy = yes
		submarine = yes
		trade = yes
	}
	is_enemy = {
		AND = {
			any_country = {
				controls_province = 1350
				OR = {
					is_in_array = { embargo_array = PREV }
					has_war_with = PREV
				}
			}
			any_country = {
				controls_province = 3543
				OR = {
					is_in_array = { embargo_array = PREV }
					has_war_with = PREV
				}
			}
			any_country = {
				controls_province = 9907
				OR = {
					is_in_array = { embargo_array = PREV }
					has_war_with = PREV
				}
			}
			any_country = {
				controls_province = 9888
				OR = {
					is_in_array = { embargo_array = PREV }
					has_war_with = PREV
				}
			}
		}
	}
	required_provinces = { 1350 3543 9907 9888 }
	icon = 9888
	offset = { -2 -2 0 }
}

adjacency_rule = {
	name = "STRAIT_DRAJKMYR_CENTRAL" #Central Drajkmyr Strait
	contested = {
		army = yes
		navy = yes
		submarine = yes
		trade = yes
	}
	enemy = {
		army = no
		navy = no
		submarine = no
		trade = no
	}
	friend = {
		army = yes
		navy = yes
		submarine = yes
		trade = yes
	}
	neutral = {
		army = yes
		navy = yes
		submarine = yes
		trade = yes
	}
	is_enemy = {
		AND = {
			any_country = {
				controls_province = 4477
				OR = {
					is_in_array = { embargo_array = PREV }
					has_war_with = PREV
				}
			}
			any_country = {
				controls_province = 4745
				OR = {
					is_in_array = { embargo_array = PREV }
					has_war_with = PREV
				}
			}
			any_country = {
				controls_province = 6356
				OR = {
					is_in_array = { embargo_array = PREV }
					has_war_with = PREV
				}
			}
			any_country = {
				controls_province = 9942
				OR = {
					is_in_array = { embargo_array = PREV }
					has_war_with = PREV
				}
			}
		}
	}
	required_provinces = { 4477 4745 6356 9942 }
	icon = 9942
	offset = { -13 -2 5 }
}

adjacency_rule = {
	name = "STRAIT_MORTHAL" #Strait of Morthal
	contested = {
		army = yes
		navy = yes
		submarine = yes
		trade = yes
	}
	enemy = {
		army = no
		navy = no
		submarine = no
		trade = no
	}
	friend = {
		army = yes
		navy = yes
		submarine = yes
		trade = yes
	}
	neutral = {
		army = yes
		navy = yes
		submarine = yes
		trade = yes
	}
	is_enemy = {
		AND = {
			any_country = {
				controls_province = 7052
				OR = {
					is_in_array = { embargo_array = PREV }
					has_war_with = PREV
				}
			}
			any_country = {
				controls_province = 7067
				OR = {
					is_in_array = { embargo_array = PREV }
					has_war_with = PREV
				}
			}
		}
	}
	required_provinces = { 7052 7067 }
	icon = 7067
	offset = { -13 -2 2 }
}