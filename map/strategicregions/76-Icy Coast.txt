strategic_region = {
	id=76
	name = "REGION_76" #Icy Coast
	provinces = {
		12405 12418 12774 12793 12856 13027 13165 13168 13215 13398 13417 13472 13486 13549 13583 13763 14071 14136 14203 14296 14302 
	}
	
	naval_terrain = water_shallow_sea
	weather = {
		period = {
			between = { 0.0 30.11 }
			temperature = { 5.0 15.0 }
			temperature_day_night = { -2.0 2.0 }
			no_phenomenon = 1.0
		}
	}
}
