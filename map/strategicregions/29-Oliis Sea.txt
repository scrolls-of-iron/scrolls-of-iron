strategic_region = {
	id=29
	name = "REGION_29" #Oliis Sea
	provinces = {
		13471 14014 14041 14066 14472 
	}
	
	naval_terrain = water_shallow_sea
	weather = {
		period = {
			between = { 0.0 30.11 }
			temperature = { 5.0 15.0 }
			temperature_day_night = { -2.0 2.0 }
			no_phenomenon = 1.0
		}
	}
}
