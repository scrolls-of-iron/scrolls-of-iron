strategic_region = {
	id=75
	name = "REGION_75" #Western Sea of Ghosts
	provinces = {
		12248 12302 12453 12515 12614 12630 12680 12816 12833 12838 12918 12931 13007 13081 13475 13565 13571 13896 14062 14238 14239 14261
	}
	
	naval_terrain = water_shallow_sea
	weather = {
		period = {
			between = { 0.0 30.11 }
			temperature = { 5.0 15.0 }
			temperature_day_night = { -2.0 2.0 }
			no_phenomenon = 1.0
		}
	}
}
