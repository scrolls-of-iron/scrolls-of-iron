strategic_region = {
	id=23
	name = "REGION_23" #South Soulrest Coast
	provinces = {
		13012 13240 13267 13913 14495 
	}
	
	naval_terrain = water_shallow_sea
	weather = {
		period = {
			between = { 0.0 30.11 }
			temperature = { 5.0 15.0 }
			temperature_day_night = { -2.0 2.0 }
			no_phenomenon = 1.0
		}
	}
}
