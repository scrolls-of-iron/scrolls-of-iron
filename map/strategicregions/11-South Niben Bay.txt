strategic_region = {
	id = 11
	name = "REGION_11" #South Niben Bay
	provinces = {
		12319 12479 14053 14135 
	}
	
	naval_terrain = water_shallow_sea
	weather = {
		period = {
			between = { 0.0 30.11 }
			temperature = { 5.0 15.0 }
			temperature_day_night = { -2.0 2.0 }
			no_phenomenon = 1.0
		}
	}
}
