strategic_region = {
	id=39
	name = "REGION_39" #Stormhold Swamp
	provinces = {
		12883 14027 14029 
	}
	
	naval_terrain = water_shallow_sea
	weather = {
		period = {
			between = { 0.0 30.11 }
			temperature = { 5.0 15.0 }
			temperature_day_night = { -2.0 2.0 }
			no_phenomenon = 1.0
		}
	}
}
