strategic_region = {
	id = 127
	name = "REGION_127" #Central Topal Bay
	provinces = {
		12085 12128 12220 12648 12957 13146 13359 13403 13582 14157 14457 
	}
	
	naval_terrain = water_shallow_sea
	weather = {
		period = {
			between = { 0.0 30.11 }
			temperature = { 5.0 15.0 }
			temperature_day_night = { -2.0 2.0 }
			no_phenomenon = 1.0
		}
	}
}
