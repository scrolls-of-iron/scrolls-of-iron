strategic_region = {
	id=60
	name = "REGION_60" #Bitter Sea
	provinces = {
		12474 12600 12626 12711 12882 13701 
	}
	
	naval_terrain = water_shallow_sea
	weather = {
		period = {
			between = { 0.0 30.11 }
			temperature = { 5.0 15.0 }
			temperature_day_night = { -2.0 2.0 }
			no_phenomenon = 1.0
		}
	}
}
