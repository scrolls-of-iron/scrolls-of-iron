strategic_region = {
	id=94
	name = "REGION_94" #Coraldale Sea
	provinces = {
		12082 12100 12432 12461 12540 12733 12887 13039 13060 13175 13493 13504 13522 13864 13897 13993 14074 14081 14160 14450 14458 14459 14465 14477 
	}
	
	naval_terrain = water_shallow_sea
	weather = {
		period = {
			between = { 0.0 30.11 }
			temperature = { 5.0 15.0 }
			temperature_day_night = { -2.0 2.0 }
			no_phenomenon = 1.0
		}
	}
}
