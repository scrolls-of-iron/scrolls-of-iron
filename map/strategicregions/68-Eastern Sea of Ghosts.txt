strategic_region = {
	id=68
	name = "REGION_68" #Eastern Sea of Ghosts
	provinces = {
		12241 12261 12357 12999 13076 13103 13124 13203 13420 13470 13492 13561 14271 14282 14293 
	}
	
	naval_terrain = water_shallow_sea
	weather = {
		period = {
			between = { 0.0 30.11 }
			temperature = { 5.0 15.0 }
			temperature_day_night = { -2.0 2.0 }
			no_phenomenon = 1.0
		}
	}
}
