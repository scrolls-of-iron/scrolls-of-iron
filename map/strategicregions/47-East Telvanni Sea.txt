strategic_region = {
	id=47
	name = "REGION_47" #East Telvanni Sea
	provinces = {
		12140 12379 12500 12516 12842 12926 13286 13349 13481 13637 13716 13963 14131 14226 14306 
	}
	
	naval_terrain = water_shallow_sea
	weather = {
		period = {
			between = { 0.0 30.11 }
			temperature = { 5.0 15.0 }
			temperature_day_night = { -2.0 2.0 }
			no_phenomenon = 1.0
		}
	}
}
